package com.kelompok1.NanemKuy.serviceTest;

//import com.kelompok1.NanemKuy.init.service.TanamanService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import java.lang.reflect.Method;
//import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TanamanServiceTest {
    private Class<?> tanamanServiceClass;

    @BeforeEach
    public void setup() throws Exception {
        tanamanServiceClass = Class.forName("com.kelompok1.NanemKuy.init.service.TanamanService");
    }

    @Test
    public void testTanamanServiceClassIsAPublicInterfacce() {
        int classModifiers = tanamanServiceClass.getModifiers();
        assertTrue(Modifier.isAbstract(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testGetListTanamanMethodInTanamanService() throws Exception{
        Method getListTanaman = tanamanServiceClass.getDeclaredMethod("getListTanaman");
        int methodModifiers = getListTanaman.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getListTanaman.getParameterCount());
    }

//    @Test
//    public void testGetListByKeyMethodInTanamanService() throws Exception{
//
//        Class[] cArg = new Class[1];
//        cArg[0] = String.class;
//        Method getListByKey = tanamanServiceClass.getDeclaredMethod("getListByKey", cArg);
//        int methodModifiers = getListByKey.getModifiers();
//        assertTrue(Modifier.isPublic(methodModifiers));
//        assertTrue(Modifier.isAbstract(methodModifiers));
//        assertEquals(1, getListByKey.getParameterCount());
//    }
}
