package com.kelompok1.NanemKuy.serviceTest;

import static org.mockito.Mockito.*;

import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.service.TanamanServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TanamanServiceImplTest {
    private Class<?> tanamanServiceImplClass;

    @InjectMocks
    private TanamanServiceImpl tanamanServiceImpl;

    @Mock
    private TanamanRepository tanamanRepository;

    @BeforeEach
    void setup() throws Exception{
        tanamanServiceImplClass = Class.forName("com.kelompok1.NanemKuy.init.service.TanamanServiceImpl");
    }

//    @Test
//    public void testCreateTanamanMethod() throws Exception{
//        int oldSize = tanamanServiceImpl.getListTanaman().size();
//        tanamanServiceImpl.createTanaman("Lidah Buaya", 70000);
//        assertEquals(oldSize+1, tanamanServiceImpl.getListTanaman().size());
//    }

    @Test
    void testGetListByKeyMethod() throws Exception{
        tanamanServiceImpl.getListByFilter("Ho", "", "");
        verify(tanamanRepository, times(1)).findByFilter("Ho", "", "");
    }
}
