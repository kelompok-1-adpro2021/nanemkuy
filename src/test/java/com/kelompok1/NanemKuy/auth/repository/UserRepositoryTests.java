package com.kelompok1.NanemKuy.auth.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import com.kelompok1.NanemKuy.auth.models.User;
import com.kelompok1.NanemKuy.auth.models.MyUserDetails;
import com.kelompok1.NanemKuy.auth.repository.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testSavedUserExist() {

        Optional<User> optionalUser = userRepository.findByUserName("rasika");
        assertNotNull(optionalUser);
        User addedUser = optionalUser.get();
        assertNotNull(addedUser);

        Optional<User> optionalUser2 = userRepository.findByUserName("admin");
        assertNotNull(optionalUser2);
        User addedUser2 = optionalUser2.get();
        assertNotNull(addedUser2);

    }
}