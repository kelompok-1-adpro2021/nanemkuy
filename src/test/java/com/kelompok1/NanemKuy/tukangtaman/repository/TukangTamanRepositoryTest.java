package com.kelompok1.NanemKuy.tukangtaman.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.kelompok1.NanemKuy.tukangtaman.core.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class TukangTamanRepositoryTest {
    private TukangTamanRepository tukangTamanRepository;

    @Mock
    private List<TukangTaman> sampleTukangTamanList;

    private TukangTaman tukangTamanSample;

    @BeforeEach
    void setUp() {
        tukangTamanSample = new TamanSejukRindang("Taman Sejuk", "Hubungi Line tmnsejuk");
        tukangTamanRepository = new TukangTamanRepository();
        sampleTukangTamanList = new ArrayList<>();
        sampleTukangTamanList.add(tukangTamanSample);
        sampleTukangTamanList.add(new TamanRumahmu("Taman Rumah", "Hubungi Line tmnrumah"));
    }

    @Test
    void testWhenGetTukangTamanListMethodIsCalledItShouldReturnTukangTamanList() {
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        List<TukangTaman> listTukangTaman = tukangTamanRepository.getListTukangTaman();
        assertEquals(listTukangTaman, sampleTukangTamanList);
    }

    @Test
    void testWhenTukangTamanRepoNotContainsCertainTukangTamanItShouldReturnNull() {
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        TukangTaman tukangTaman = tukangTamanRepository.getTukangTaman("Taman Jaya");
        assertEquals(null, tukangTaman);
    }

    @Test
    void testWhenAddTukangTamanMethodIsCalledItShouldAddTukangTamanToList() {
        int sizeTukangTamanListLama = sampleTukangTamanList.size();
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        tukangTamanRepository.addTukangTaman(
                new TamanAsri("Taman Asri", "Hubungi Line tmnasri"));
        int sizeTukangTamanListBaru = sampleTukangTamanList.size();
        assertEquals(sizeTukangTamanListLama + 1, sizeTukangTamanListBaru);
    }

    @Test
    void testWhenGetTukangTamanByNamaMethodIsCalledItShouldReturnCorrectTukangTaman() {
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        TukangTaman tukangTaman = tukangTamanRepository.getTukangTaman("Taman Sejuk");
        assertEquals(tukangTaman, tukangTamanSample);
    }

    @Test
    void testWhenDeleteTukangTamanMethodIsCalledItShouldDeleteTukangTamanFromList() {
        int sizeTukangTamanListLama = sampleTukangTamanList.size();
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        tukangTamanRepository.deleteTukangTaman(tukangTamanSample);
        int sizeTukangTamanListBaru = sampleTukangTamanList.size();
        assertEquals(sizeTukangTamanListLama - 1, sizeTukangTamanListBaru);
    }
}
