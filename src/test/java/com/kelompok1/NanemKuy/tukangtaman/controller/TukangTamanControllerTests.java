package com.kelompok1.NanemKuy.tukangtaman.controller;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.NanemKuy.tukangtaman.core.*;
import com.kelompok1.NanemKuy.tukangtaman.service.TukangTamanService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TukangTamanControllerTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private TukangTamanService tukangTamanService;

    private TukangTaman tukangtaman;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        tukangtaman = new TamanAsri("Taman Indah", "deskripsi");
    }

    private List<TukangTaman> generateTukangTamans() {
        List<TukangTaman> tukangtamans = new ArrayList<>();
        tukangtamans.add(new TamanAsri("Taman Asri", "deskripsi"));
        tukangtamans.add(new TamanHijauDaun("Taman Hijau", "deskripsi"));
        tukangtamans.add(new TamanJayaAgung("Taman Jaya", "deskripsi"));
        tukangtamans.add(new TamanRumahmu("Taman Rumahmu", "deskripsi"));
        tukangtamans.add(new TamanSejukRindang("Taman Sejuk", "deskripsi"));
        return tukangtamans;
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testUrlTukangTaman() throws Exception {
        List<TukangTaman> tukangtamans = generateTukangTamans();
        when(tukangTamanService.getListTukangTaman()).thenReturn(tukangtamans);

        mockMvc.perform(get("/tukang-taman"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("listtukangtaman", hasSize(5)));
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testUrlDetailTukangTaman() throws Exception {
        when(tukangTamanService.getTukangTaman("Taman Indah")).thenReturn(tukangtaman);

        mockMvc.perform(get("/tukang-taman/detail/" + tukangtaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("detailTukangTaman"))
                .andExpect(model().attributeExists("tukangtaman"))
                .andExpect(view().name("tukangtaman/detailTukangTaman"));
        verify(tukangTamanService, times(1)).getTukangTaman("Taman Indah");
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenCheckTanamanInWishlistUrlIsAccessed() throws Exception {
        when(tukangTamanService
                .addReviewToTukangTaman(tukangtaman.getNama(), "Nadia", "Bagus banget"))
                .thenReturn("Bagus banget");

        mockMvc.perform(post("/tukang-taman/detail/" + tukangtaman.getNama() + "/add-review")
                .param("nama", "Nadia")
                .param("komentar", "Bagus banget"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addReview"))
                .andExpect(result -> assertEquals(
                        "Bagus banget", result.getResponse().getContentAsString()));
        verify(tukangTamanService, times(1))
                .addReviewToTukangTaman("Taman Indah", "Nadia", "Bagus banget");
    }
}
