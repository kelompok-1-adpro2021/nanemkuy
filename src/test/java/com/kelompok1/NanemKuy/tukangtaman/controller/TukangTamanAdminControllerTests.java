package com.kelompok1.NanemKuy.tukangtaman.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.NanemKuy.tukangtaman.core.*;
import com.kelompok1.NanemKuy.tukangtaman.service.TukangTamanService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TukangTamanAdminControllerTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private TukangTamanService tukangTamanService;

    private TukangTaman tukangtaman;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        tukangtaman = new TamanAsri("Taman Indah", "deskripsi");
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testListTukangTamanUrl() throws Exception {
        List<TukangTaman> listTukangTaman = new ArrayList<>();
        listTukangTaman.add(tukangtaman);
        when(tukangTamanService.getListTukangTaman()).thenReturn(listTukangTaman);

        mockMvc.perform(get("/tukang-taman/admin/list-tukang-taman/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("listTukangTaman"))
                .andExpect(model().attributeExists("listTukangTaman"))
                .andExpect(view().name("tukangtaman/list-tukang-taman"));
        verify(tukangTamanService, times(1)).getListTukangTaman();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testAddTukangTamanUrl() throws Exception {
        mockMvc.perform(get("/tukang-taman/admin/add-tukang-taman"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addTukangTaman"))
                .andExpect(model().attributeExists("kategoriTaman"))
                .andExpect(model().attribute("kategoriTaman", hasSize(5)))
                .andExpect(view().name("tukangtaman/add-tukang-taman"));
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testPostAddTukangTamanUrl() throws Exception {
        mockMvc.perform(post("/tukang-taman/admin/add-tukang-taman")
                .param("namaTukangTaman", "Hijau Asri")
                .param("deskripsiTukangTaman", "Hubungi WA 081122223333")
                .param("kategoriTukangTaman", "Taman Minimalis"))
                .andExpect(handler().methodName("postAddTukangTaman"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tukang-taman/admin/list-tukang-taman"));
        verify(tukangTamanService, times(1))
                .addTukangTaman("Hijau Asri", "Hubungi WA 081122223333", "Taman Minimalis");
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testUpdateTukangTamanUrl() throws Exception {
        when(tukangTamanService.getTukangTaman(tukangtaman.getNama())).thenReturn(tukangtaman);

        mockMvc.perform(get("/tukang-taman/admin/update-tukang-taman/" + tukangtaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("updateTukangTaman"))
                .andExpect(model().attributeExists("tukangTaman"))
                .andExpect(view().name("tukangtaman/update-tukang-taman"));
        verify(tukangTamanService, times(1)).getTukangTaman("Taman Indah");
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testPostUpdateTukangTamanUrl() throws Exception {
        when(tukangTamanService.getTukangTaman(tukangtaman.getNama())).thenReturn(tukangtaman);

        mockMvc.perform(post("/tukang-taman/admin/update-tukang-taman/" + tukangtaman.getNama())
                .param("namaTukangTaman", "Hijau Asri")
                .param("deskripsiTukangTaman", "Hubungi WA 081122223333")
                .param("kategoriTukangTaman", "Taman Minimalis"))
                .andExpect(handler().methodName("postUpdateTukangTaman"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tukang-taman/admin/list-tukang-taman"));
        verify(tukangTamanService, times(1))
                .updateTukangTaman("Taman Indah", "Hijau Asri", "Hubungi WA 081122223333");
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testDeleteTukangTamanUrl() throws Exception {
        when(tukangTamanService.getTukangTaman(tukangtaman.getNama())).thenReturn(tukangtaman);

        mockMvc.perform(get("/tukang-taman/admin/delete-tukang-taman/" + tukangtaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("deleteTukangTaman"))
                .andExpect(model().attributeExists("tukangTaman"))
                .andExpect(view().name("tukangtaman/delete-tukang-taman"));
        verify(tukangTamanService, times(1)).getTukangTaman("Taman Indah");
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testPostDeleteTukangTamanUrl() throws Exception {
        when(tukangTamanService.getTukangTaman(tukangtaman.getNama())).thenReturn(tukangtaman);

        mockMvc.perform(post("/tukang-taman/admin/delete-tukang-taman/" + tukangtaman.getNama()))
                .andExpect(handler().methodName("postDeleteTukangTaman"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tukang-taman/admin/list-tukang-taman"));
        verify(tukangTamanService, times(1)).deleteTukangTaman("Taman Indah");
    }
}
