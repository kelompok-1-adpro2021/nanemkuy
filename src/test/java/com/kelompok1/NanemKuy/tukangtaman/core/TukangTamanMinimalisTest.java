package com.kelompok1.NanemKuy.tukangtaman.core;

import com.kelompok1.NanemKuy.tukangtaman.core.TukangTamanMinimalis;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class TukangTamanMinimalisTest {

    private Class<?> tukangTamanMinimalisClass;
    private TukangTamanMinimalis tukangTamanMinimalis;

    @BeforeEach
    public void setUp() throws Exception {
        tukangTamanMinimalisClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TukangTamanMinimalis");
        tukangTamanMinimalis = new TukangTamanMinimalis();
    }

    @Test
    public void testTukangTamanMinimalisIsKategori() {
        Collection<Type> interfaces = Arrays.asList(tukangTamanMinimalisClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.tukangtaman.core.Kategori")));
    }

    @Test
    public void testTukangTamanMinimalisOverrideGetRincianMethod() throws Exception {
        Method getRincian = tukangTamanMinimalisClass.getDeclaredMethod("getRincian");

        assertTrue(Modifier.isPublic(getRincian.getModifiers()));
        assertEquals("java.lang.String", getRincian.getGenericReturnType().getTypeName());
    }

    @Test
    public void testTukangTamanMinimalisGetRincianMethodReturnCorrectly() {
        assertEquals("Siap membantu membuat taman minimalis di rumahmu.", tukangTamanMinimalis.getRincian());
    }
}
