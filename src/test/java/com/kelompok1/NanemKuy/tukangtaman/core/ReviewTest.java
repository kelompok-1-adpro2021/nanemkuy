package com.kelompok1.NanemKuy.tukangtaman.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ReviewTest {

    private Class<?> reviewClass;

    private Review review;

    @BeforeEach
    public void setUp() throws Exception {
        reviewClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.Review");
        review = new Review("Nenek", "sukak deh");

    }

    @Test
    public void testReviewGetNamaWorkCorrectly() throws Exception {
        Method getNama = reviewClass.getDeclaredMethod("getNama");

        assertTrue(Modifier.isPublic(getNama.getModifiers()));
        assertEquals(0, getNama.getParameterCount());
        assertEquals("java.lang.String", getNama.getGenericReturnType().getTypeName());

        assertEquals("Nenek", review.getNama());
    }

    @Test
    public void testReviewGetKomentarWorkCorrectly() throws Exception {
        Method getKomentar = reviewClass.getDeclaredMethod("getKomentar");

        assertTrue(Modifier.isPublic(getKomentar.getModifiers()));
        assertEquals(0, getKomentar.getParameterCount());
        assertEquals("java.lang.String", getKomentar.getGenericReturnType().getTypeName());

        assertEquals("sukak deh", review.getKomentar());
    }


}

