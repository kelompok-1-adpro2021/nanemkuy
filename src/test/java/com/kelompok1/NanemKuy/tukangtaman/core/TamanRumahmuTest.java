package com.kelompok1.NanemKuy.tukangtaman.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TamanRumahmuTest {
    private Class<?> tamanRumahmuClass;
    private TamanRumahmu tamanRumahmu;

    @BeforeEach
    public void setUp() throws Exception {
        tamanRumahmuClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TamanRumahmu");
        tamanRumahmu = new TamanRumahmu("Taman Rumahmu", "deskripsi");
    }

    @Test
    public void testTamanRumahmuIsConcreteClass() {
        assertFalse(Modifier.isAbstract(tamanRumahmuClass.getModifiers()));
    }

    @Test
    public void testTamanRumahmuIsTukangTaman() {
        Class<?> parentClass = tamanRumahmuClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman",
                parentClass.getTypeName());
    }

    @Test
    public void testTamanRumahmuDefaultConstructorWorkCorrectly() {
        assertEquals("Taman Rumahmu", tamanRumahmu.getNama());
        assertEquals("deskripsi", tamanRumahmu.getDeskripsi());
        assertEquals("Siap membantu membuat taman minimalis di rumahmu.", tamanRumahmu.getKategori());
    }
}
