package com.kelompok1.NanemKuy.tukangtaman.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TamanJayaAgungTest {
    private Class<?> tamanJayaAgungClass;
    private TamanJayaAgung tamanJayaAgung;

    @BeforeEach
    public void setUp() throws Exception {
        tamanJayaAgungClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TamanJayaAgung");
        tamanJayaAgung = new TamanJayaAgung("Taman Jaya Agung", "deskripsi");
    }

    @Test
    public void testTamanJayaAgungIsConcreteClass() {
        assertFalse(Modifier.isAbstract(tamanJayaAgungClass.getModifiers()));
    }

    @Test
    public void testTamanJayaAgungIsTukangTaman() {
        Class<?> parentClass = tamanJayaAgungClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman",
                parentClass.getTypeName());
    }

    @Test
    public void testTamanJayaAgungDefaultConstructorWorkCorrectly() {
        assertEquals("Taman Jaya Agung", tamanJayaAgung.getNama());
        assertEquals("deskripsi", tamanJayaAgung.getDeskripsi());
        assertEquals("Menyediakan jasa pembuatan kolam di taman loh", tamanJayaAgung.getKategori());
    }
}
