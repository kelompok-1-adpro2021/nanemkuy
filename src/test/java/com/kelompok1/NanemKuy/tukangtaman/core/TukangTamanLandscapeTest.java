package com.kelompok1.NanemKuy.tukangtaman.core;

import com.kelompok1.NanemKuy.tukangtaman.core.TukangTamanLandscape;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class TukangTamanLandscapeTest {

    private Class<?> tukangTamanLandscapeClass;
    private TukangTamanLandscape tukangTamanLandscape;

    @BeforeEach
    public void setUp() throws Exception {
        tukangTamanLandscapeClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TukangTamanLandscape");
        tukangTamanLandscape = new TukangTamanLandscape();
    }

    @Test
    public void testTukangTamanLandscapeIsKategori() {
        Collection<Type> interfaces = Arrays.asList(tukangTamanLandscapeClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.tukangtaman.core.Kategori")));
    }

    @Test
    public void testTukangTamanLandscapeOverrideGetRincianMethod() throws Exception {
        Method getRincian = tukangTamanLandscapeClass.getDeclaredMethod("getRincian");

        assertTrue(Modifier.isPublic(getRincian.getModifiers()));
        assertEquals("java.lang.String", getRincian.getGenericReturnType().getTypeName());
    }

    @Test
    public void testTukangTamanLandscapeGetRincianMethodReturnCorrectly() {
        assertEquals("Jasa membuat taman lansekap untuk halaman depan dan belakang rumah!", tukangTamanLandscape.getRincian());
    }
}
