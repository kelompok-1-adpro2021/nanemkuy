package com.kelompok1.NanemKuy.tukangtaman.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TamanAsriTest {
    private Class<?> tamanAsriClass;
    private TamanAsri tamanAsri;

    @BeforeEach
    public void setUp() throws Exception {
        tamanAsriClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TamanAsri");
        tamanAsri = new TamanAsri("Taman Asri", "deskripsi");
    }

    @Test
    public void testTamanAsriIsConcreteClass() {
        assertFalse(Modifier.isAbstract(tamanAsriClass.getModifiers()));
    }

    @Test
    public void testTamanAsriIsTukangTaman() {
        Class<?> parentClass = tamanAsriClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman",
                parentClass.getTypeName());
    }

    @Test
    public void testTamanAsriDefaultConstructorWorkCorrectly() {
        assertEquals("Taman Asri", tamanAsri.getNama());
        assertEquals("deskripsi", tamanAsri.getDeskripsi());
        assertEquals("Siap membantu membuat taman minimalis di rumahmu.", tamanAsri.getKategori());
    }
}
