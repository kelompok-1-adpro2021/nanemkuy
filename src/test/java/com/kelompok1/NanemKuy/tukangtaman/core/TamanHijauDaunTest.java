package com.kelompok1.NanemKuy.tukangtaman.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TamanHijauDaunTest {
    private Class<?> tamanHijauDaunClass;
    private TamanHijauDaun tamanHijauDaun;

    @BeforeEach
    public void setUp() throws Exception {
        tamanHijauDaunClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TamanHijauDaun");
        tamanHijauDaun = new TamanHijauDaun("Taman Hijau Daun", "deskripsi");
    }

    @Test
    public void testTamanHijauDaunIsConcreteClass() {
        assertFalse(Modifier.isAbstract(tamanHijauDaunClass.getModifiers()));
    }

    @Test
    public void testTamanHijauDaunIsTukangTaman() {
        Class<?> parentClass = tamanHijauDaunClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman",
                parentClass.getTypeName());
    }

    @Test
    public void testTamanHijauDaunDefaultConstructorWorkCorrectly() {
        assertEquals("Taman Hijau Daun", tamanHijauDaun.getNama());
        assertEquals("deskripsi", tamanHijauDaun.getDeskripsi());
        assertEquals("Jasa membuat taman lansekap untuk halaman depan dan belakang rumah!", tamanHijauDaun.getKategori());
    }
}
