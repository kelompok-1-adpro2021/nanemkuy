package com.kelompok1.NanemKuy.tukangtaman.core;

import com.kelompok1.NanemKuy.tukangtaman.core.TukangTamanKolam;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class TukangTamanKolamTest {

    private Class<?> tukangTamanKolamClass;
    private TukangTamanKolam tukangTamanKolam;

    @BeforeEach
    public void setUp() throws Exception {
        tukangTamanKolamClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TukangTamanKolam");
        tukangTamanKolam = new TukangTamanKolam();
    }

    @Test
    public void testTukangTamanKolamIsKategori() {
        Collection<Type> interfaces = Arrays.asList(tukangTamanKolamClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.tukangtaman.core.Kategori")));
    }

    @Test
    public void testTukangTamanKolamOverrideGetRincianMethod() throws Exception {
        Method getRincian = tukangTamanKolamClass.getDeclaredMethod("getRincian");

        assertTrue(Modifier.isPublic(getRincian.getModifiers()));
        assertEquals("java.lang.String", getRincian.getGenericReturnType().getTypeName());
    }

    @Test
    public void testTukangTamanKolamGetRincianMethodReturnCorrectly() {
        assertEquals("Menyediakan jasa pembuatan kolam di taman loh", tukangTamanKolam.getRincian());
    }
}
