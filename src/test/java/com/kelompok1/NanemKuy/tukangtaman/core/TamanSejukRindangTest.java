package com.kelompok1.NanemKuy.tukangtaman.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TamanSejukRindangTest {
    private Class<?> tamanSejukRindangClass;
    private TamanSejukRindang tamanSejukRindang;

    @BeforeEach
    public void setUp() throws Exception {
        tamanSejukRindangClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TamanSejukRindang");
        tamanSejukRindang = new TamanSejukRindang("Taman Sejuk Rindang", "deskripsi");
    }

    @Test
    public void testTamanSejukRindangIsConcreteClass() {
        assertFalse(Modifier.isAbstract(tamanSejukRindangClass.getModifiers()));
    }

    @Test
    public void testTamanSejukRindangIsTukangTaman() {
        Class<?> parentClass = tamanSejukRindangClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman",
                parentClass.getTypeName());
    }

    @Test
    public void testTamanSejukRindangDefaultConstructorWorkCorrectly() {
        assertEquals("Taman Sejuk Rindang", tamanSejukRindang.getNama());
        assertEquals("deskripsi", tamanSejukRindang.getDeskripsi());
        assertEquals("Menyediakan jasa pembuatan kolam di taman loh", tamanSejukRindang.getKategori());
    }
}
