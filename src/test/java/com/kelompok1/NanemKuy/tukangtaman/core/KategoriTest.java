package com.kelompok1.NanemKuy.tukangtaman.core;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class KategoriTest {

    private Class<?> kategoriClass;

    @BeforeEach
    public void setUp() throws Exception {
        kategoriClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.Kategori");
    }

    @Test
    public void testKategoriClassIsAPublicInterface() {
        int classModifier = kategoriClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }

    @Test
    public void testKategoriHasGetRincianMethod() throws Exception {
        Method getRincian = kategoriClass.getDeclaredMethod("getRincian");
        int methodModifier = kategoriClass.getModifiers();

        assertTrue(Modifier.isPublic(methodModifier));
        assertEquals(0, getRincian.getParameterCount());
        assertEquals("java.lang.String", getRincian.getGenericReturnType().getTypeName());
    }
}
