package com.kelompok1.NanemKuy.tukangtaman.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TukangTamanTest {

    private Class<?> tukangTamanClass;

    private TukangTaman tukangTaman;

    @BeforeEach
    public void setUp() throws Exception {
        tukangTamanClass = Class.forName("com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman");
        tukangTaman = new TamanAsri("Taman Asri", "deskripsi");

    }

    @Test
    public void testTukangTamanIsAbstract() {
        assertTrue(Modifier.isAbstract(tukangTamanClass.getModifiers()));}


    @Test
    public void testTukangTamanGetNamaWorkCorrectly() throws Exception {
        Method getNama = tukangTamanClass.getDeclaredMethod("getNama");

        assertTrue(Modifier.isPublic(getNama.getModifiers()));
        assertEquals(0, getNama.getParameterCount());
        assertEquals("java.lang.String", getNama.getGenericReturnType().getTypeName());

        assertEquals("Taman Asri", tukangTaman.getNama());
    }

    @Test
    public void testTukangTamanSetNamaWorkCorrectly() throws Exception {
        Method setNama = tukangTamanClass.getDeclaredMethod("setNama", String.class);

        assertTrue(Modifier.isPublic(setNama.getModifiers()));
        assertEquals(1, setNama.getParameterCount());
        assertEquals("void", setNama.getGenericReturnType().getTypeName());

        tukangTaman.setNama("Taman Asri Welas");
        assertEquals("Taman Asri Welas", tukangTaman.getNama());
    }

    @Test
    public void testTukangTamanGetDeskripsiWorkCorrectly() throws Exception {
        Method getDeskripsi = tukangTamanClass.getDeclaredMethod("getDeskripsi");

        assertTrue(Modifier.isPublic(getDeskripsi.getModifiers()));
        assertEquals(0, getDeskripsi.getParameterCount());
        assertEquals("java.lang.String", getDeskripsi.getGenericReturnType().getTypeName());

        assertEquals("deskripsi", tukangTaman.getDeskripsi());
    }

    @Test
    public void testTukangTamanSetDeskripsiWorkCorrectly() throws Exception {
        Method setDeskripsi = tukangTamanClass.getDeclaredMethod("setDeskripsi", String.class);

        assertTrue(Modifier.isPublic(setDeskripsi.getModifiers()));
        assertEquals(1, setDeskripsi.getParameterCount());
        assertEquals("void", setDeskripsi.getGenericReturnType().getTypeName());

        tukangTaman.setDeskripsi("deskripsi changed");
        assertEquals("deskripsi changed", tukangTaman.getDeskripsi());
    }

    @Test
    public void testTukangTamanGetKategoriWorkCorrectly() throws Exception {
        Method getKategori = tukangTamanClass.getDeclaredMethod("getKategori");

        assertTrue(Modifier.isPublic(getKategori.getModifiers()));
        assertEquals(0, getKategori.getParameterCount());
        assertEquals("java.lang.String", getKategori.getGenericReturnType().getTypeName());

        assertEquals("Siap membantu membuat taman minimalis di rumahmu.",
                tukangTaman.getKategori());
    }

    @Test
    public void testTukangTamanGetKategoriInKategoriTypeWorkCorrectly() throws Exception {
        Method getKategoriInKategoriType =
                tukangTamanClass.getDeclaredMethod("getKategoriInKategoriType");

        assertTrue(Modifier.isPublic(getKategoriInKategoriType.getModifiers()));
        assertEquals(0, getKategoriInKategoriType.getParameterCount());

        assertEquals(TukangTamanMinimalis.class, tukangTaman.getKategoriInKategoriType().getClass());
    }

    @Test
    public void testTukangTamanSetKategoriWorkCorrectly() throws Exception {
        Method setKategori = tukangTamanClass.getDeclaredMethod("setKategori", Kategori.class);

        assertTrue(Modifier.isPublic(setKategori.getModifiers()));
        assertEquals(1, setKategori.getParameterCount());
        assertEquals("void", setKategori.getGenericReturnType().getTypeName());

        Kategori newKategori = new TukangTamanLandscape();
        tukangTaman.setKategori(newKategori);
        assertEquals("Jasa membuat taman lansekap untuk halaman depan dan belakang rumah!",
                tukangTaman.getKategori());
    }

    @Test
    public void testTukangTamanGetListReviewWorkCorrectly() throws Exception {
        Method getListReview = tukangTamanClass.getDeclaredMethod("getListReview");

        assertTrue(Modifier.isPublic(getListReview.getModifiers()));
        assertEquals(0, getListReview.getParameterCount());

        Review review = new Review ("Nana", "bagus");
        Review reviewAdded = tukangTaman.addReview(review);

        List<Review> listReview = tukangTaman.getListReview();
        Review firstReview = listReview.get(0);

        assertEquals("com.kelompok1.NanemKuy.tukangtaman.core.Review",
                firstReview.getClass().getName());


    }

    @Test
    public void testTukangTamanAddReviewWorkCorrectly() throws Exception {
        Method addReview = tukangTamanClass.getDeclaredMethod("addReview", Review.class);

        assertTrue(Modifier.isPublic(addReview.getModifiers()));
        assertEquals(1, addReview.getParameterCount());

        int reviewLengthBefore = tukangTaman.getListReview().size();

        Review review = new Review ("Nano", "keren");
        Review reviewAdded = tukangTaman.addReview(review);

        int reviewLengthAfter = tukangTaman.getListReview().size();

        assertEquals(reviewLengthAfter, reviewLengthBefore + 1);
    }

}

