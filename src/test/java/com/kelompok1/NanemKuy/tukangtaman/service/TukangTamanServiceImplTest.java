package com.kelompok1.NanemKuy.tukangtaman.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.kelompok1.NanemKuy.tukangtaman.core.*;
import com.kelompok1.NanemKuy.tukangtaman.repository.TukangTamanRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TukangTamanServiceImplTest {
    @Mock
    private TukangTamanRepository tukangTamanRepository;

    @InjectMocks
    private TukangTamanServiceImpl tukangTamanService;

    private List<TukangTaman> tukangtamans;

    private TukangTaman tukangtaman;

    @BeforeEach
    void setUp() {
        tukangtaman = new TamanAsri("Taman Asri", "deskripsi");

        tukangtamans = new ArrayList<>();
        tukangtamans.add(tukangtaman);
        tukangtamans.add(new TamanHijauDaun("Taman Hijau", "deskripsi"));
        tukangtamans.add(new TamanJayaAgung("Taman Jaya", "deskripsi"));
        tukangtamans.add(new TamanRumahmu("Taman Rumahmu", "deskripsi"));
        tukangtamans.add(new TamanSejukRindang("Taman Sejuk", "deskripsi"));
    }

    @Test
    void testWhenGetListTukangTamanMethodIsCalledItShouldReturnListTukangTaman() {
        when(tukangTamanRepository.getListTukangTaman()).thenReturn(tukangtamans);
        List<TukangTaman> tukangTamanList = tukangTamanService.getListTukangTaman();
        verify(tukangTamanRepository, times(1)).getListTukangTaman();
        assertEquals(5, tukangTamanList.size());
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanAsri() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Asri");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanAsri.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanHijauDaun() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Hijau Daun");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanHijauDaun.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanJayaAgung() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Jaya Agung");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanJayaAgung.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanRumahmu() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Rumahmu");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanRumahmu.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanSejukRindang() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Sejuk Rindang");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanSejukRindang.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriNotDefine() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Yang Tidak Ada");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(null);
    }

    @Test
    void testGetTukangTamanIsReturnTheRightTukangTaman() {
        when(tukangTamanRepository.getTukangTaman("Taman Asri")).thenReturn(tukangtaman);
        TukangTaman tukangTaman = tukangTamanService.getTukangTaman("Taman Asri");
        verify(tukangTamanRepository, times(1)).getTukangTaman("Taman Asri");

        assertEquals("Taman Asri", tukangTaman.getNama());
        assertEquals("deskripsi", tukangTaman.getDeskripsi());
    }

    @Test
    void testTukangTamanReviewIsUpdatedWhenAddReview() {
        when(tukangTamanRepository.getTukangTaman("Taman Asri")).thenReturn(tukangtaman);
        TukangTaman tukangTaman = tukangTamanService.getTukangTaman("Taman Asri");
        verify(tukangTamanRepository, times(1)).getTukangTaman("Taman Asri");

        List<Review> tukangTamanReview = tukangTaman.getListReview();
        assertTrue(tukangTamanReview.isEmpty());

        String resultReview = tukangTamanService.addReviewToTukangTaman("Taman Asri",
                "Rasika", "Love it");
        assertFalse(tukangTamanReview.isEmpty());
        assertEquals(1, tukangTamanReview.size());
    }

    @Test
    void testMethodReturnRightStringWhenAddReviewToTukangTaman() {
        when(tukangTamanRepository.getTukangTaman("Taman Asri")).thenReturn(tukangtaman);
        TukangTaman tukangTaman = tukangTamanService.getTukangTaman("Taman Asri");
        verify(tukangTamanRepository, times(1)).getTukangTaman("Taman Asri");

        String resultReview = tukangTamanService.addReviewToTukangTaman(
                "Taman Asri", "Rasika", "Love it");
        assertEquals("Rasika!inibatas!Love it", resultReview);
    }

    @Test
    void testUpdateTukangTamanMethod() {
        when(tukangTamanRepository.getTukangTaman("Taman Asri")).thenReturn(tukangtaman);

        tukangTamanService.updateTukangTaman("Taman Asri", "Taman Asri Sejuk", "Telp 0812");

        verify(tukangTamanRepository, times(1)).getTukangTaman("Taman Asri");
    }

    @Test
    void testDeleteTukangTamanMethod() {
        when(tukangTamanRepository.getTukangTaman("Taman Asri")).thenReturn(tukangtaman);

        tukangTamanService.deleteTukangTaman("Taman Asri");

        verify(tukangTamanRepository, times(1)).getTukangTaman("Taman Asri");
        verify(tukangTamanRepository, times(1)).deleteTukangTaman(tukangtaman);
    }
}
