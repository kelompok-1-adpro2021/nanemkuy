package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.CalatheaFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalatheaFactoryTest {
    private Class<?> calatheaFactoryClass;
    private CalatheaFactory calatheaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        calatheaFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.CalatheaFactory");
        calatheaFactory = new CalatheaFactory();
    }

    @Test
    public void testCalatheaFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(calatheaFactoryClass.getModifiers()));
    }

    @Test
    public void testCalatheaFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(calatheaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testCalatheaFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = calatheaFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testCalatheaFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = calatheaFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testCalatheaFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = calatheaFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirSedang");
        assertEquals(air.getKebutuhanAir(), "Sedang");
    }

    @Test
    public void testCalatheaFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = calatheaFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaSedang");
        assertEquals(cahaya.getKebutuhanCahaya(), "Sedang");
    }
}
