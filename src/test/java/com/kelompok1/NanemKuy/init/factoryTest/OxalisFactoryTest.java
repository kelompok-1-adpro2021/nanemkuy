package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.OxalisFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OxalisFactoryTest {
    private Class<?> oxalisFactoryClass;
    private OxalisFactory oxalisFactory;

    @BeforeEach
    public void setUp() throws Exception {
        oxalisFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.OxalisFactory");
        oxalisFactory = new OxalisFactory();
    }

    @Test
    public void testOxalisFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(oxalisFactoryClass.getModifiers()));
    }

    @Test
    public void testOxalisFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(oxalisFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testOxalisFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = oxalisFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testOxalisFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = oxalisFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testOxalisFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = oxalisFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi");
        assertEquals(air.getKebutuhanAir(), "Tinggi");
    }

    @Test
    public void testOxalisFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = oxalisFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaSedang");
        assertEquals(cahaya.getKebutuhanCahaya(), "Sedang");
    }
}
