package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.HortensiaFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HortensiaFactoryTest {
    private Class<?> hortensiaFactoryClass;
    private HortensiaFactory hortensiaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        hortensiaFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.HortensiaFactory");
        hortensiaFactory = new HortensiaFactory();
    }

    @Test
    public void testHortensiaFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(hortensiaFactoryClass.getModifiers()));
    }

    @Test
    public void testHortensiaFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(hortensiaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testHortensiaFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = hortensiaFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testHortensiaFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = hortensiaFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testHortensiaFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = hortensiaFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi");
        assertEquals(air.getKebutuhanAir(), "Tinggi");
    }

    @Test
    public void testHortensiaFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = hortensiaFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi");
        assertEquals(cahaya.getKebutuhanCahaya(), "Tinggi");
    }
}
