package com.kelompok1.NanemKuy.init.factoryTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FactoryTest {
    private Class<?> factoryClass;

    /**
     * setUp the class Object
     * @throws ClassNotFoundException self-explanatory
     */
    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        factoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.Factory");
    }

    @Test
    public void testFactoryIsAPublicInterface() {
        int classModifiers = factoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testFactoryHasCreateKebutuhanAirAbstractMethod() throws Exception {
        Method createKebutuhanAir = factoryClass.getDeclaredMethod("createKebutuhanAir");
        int methodModifiers = createKebutuhanAir.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createKebutuhanAir.getParameterCount());
    }

    @Test
    public void testFactoryHasCreateKebutuhanCahayaAbstractMethod() throws Exception {
        Method createKebutuhanCahaya = factoryClass.getDeclaredMethod("createKebutuhanCahaya");
        int methodModifiers = createKebutuhanCahaya.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createKebutuhanCahaya.getParameterCount());
    }

}
