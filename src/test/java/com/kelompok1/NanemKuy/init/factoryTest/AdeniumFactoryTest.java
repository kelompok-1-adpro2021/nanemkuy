package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.AdeniumFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdeniumFactoryTest {
    private Class<?> adeniumFactoryClass;
    private AdeniumFactory adeniumFactory;

    @BeforeEach
    public void setUp() throws Exception {
        adeniumFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.AdeniumFactory");
        adeniumFactory = new AdeniumFactory();
    }

    @Test
    public void testAdeniumFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(adeniumFactoryClass.getModifiers()));
    }

    @Test
    public void testAdeniumFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(adeniumFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testAdeniumFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = adeniumFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testAdeniumFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = adeniumFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testAdeniumFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = adeniumFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah");
        assertEquals(air.getKebutuhanAir(), "Rendah");
    }

    @Test
    public void testAdeniumFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = adeniumFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi");
        assertEquals(cahaya.getKebutuhanCahaya(), "Tinggi");
    }


}
