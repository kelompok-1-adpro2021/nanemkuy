package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.PhilodendronFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PhilodendronFactoryTest {
    private Class<?> philodendronFactoryClass;
    private PhilodendronFactory philodendronFactory;

    @BeforeEach
    public void setUp() throws Exception {
        philodendronFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.PhilodendronFactory");
        philodendronFactory = new PhilodendronFactory();
    }

    @Test
    public void testPhilodendronFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(philodendronFactoryClass.getModifiers()));
    }

    @Test
    public void testPhilodendronFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(philodendronFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testPhilodendronFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = philodendronFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testPhilodendronFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = philodendronFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testPhilodendronFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = philodendronFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirSedang");
        assertEquals(air.getKebutuhanAir(), "Sedang");
    }

    @Test
    public void testPhilodendronFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = philodendronFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaRendah");
        assertEquals(cahaya.getKebutuhanCahaya(), "Rendah");
    }
}
