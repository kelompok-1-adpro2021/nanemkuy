package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.HomalomenaMaggieFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HomalomenaMaggieFactoryTest {
    private Class<?> homalomenaMaggieFactoryClass;
    private HomalomenaMaggieFactory homalomenaMaggieFactory;

    @BeforeEach
    public void setUp() throws Exception {
        homalomenaMaggieFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.HomalomenaMaggieFactory");
        homalomenaMaggieFactory = new HomalomenaMaggieFactory();
    }

    @Test
    public void testHomalomenaMaggieFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(homalomenaMaggieFactoryClass.getModifiers()));
    }

    @Test
    public void testHomalomenaMaggieFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(homalomenaMaggieFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testHomalomenaMaggieFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = homalomenaMaggieFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testHomalomenaMaggieFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = homalomenaMaggieFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testHomalomenaMaggieFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = homalomenaMaggieFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah");
        assertEquals(air.getKebutuhanAir(), "Rendah");
    }

    @Test
    public void testHomalomenaMaggieFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = homalomenaMaggieFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaRendah");
        assertEquals(cahaya.getKebutuhanCahaya(), "Rendah");
    }
}
