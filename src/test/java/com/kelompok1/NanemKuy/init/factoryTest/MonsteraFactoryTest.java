package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.MonsteraFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MonsteraFactoryTest {
    private Class<?> monsteraFactoryClass;
    private MonsteraFactory monsteraFactory;

    @BeforeEach
    public void setUp() throws Exception {
        monsteraFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.MonsteraFactory");
        monsteraFactory = new MonsteraFactory();
    }

    @Test
    public void testMonsteraFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(monsteraFactoryClass.getModifiers()));
    }

    @Test
    public void testMonsteraFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(monsteraFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testMonsteraFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = monsteraFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testMonsteraFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = monsteraFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testMonsteraFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = monsteraFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi");
        assertEquals(air.getKebutuhanAir(), "Tinggi");
    }

    @Test
    public void testMonsteraFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = monsteraFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaRendah");
        assertEquals(cahaya.getKebutuhanCahaya(), "Rendah");
    }
}
