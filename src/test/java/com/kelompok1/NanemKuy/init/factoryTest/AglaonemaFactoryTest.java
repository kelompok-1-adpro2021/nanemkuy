package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.AglaonemaFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AglaonemaFactoryTest {
    private Class<?> aglaonemaFactoryClass;
    private AglaonemaFactory aglaonemaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        aglaonemaFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.AglaonemaFactory");
        aglaonemaFactory = new AglaonemaFactory();
    }

    @Test
    public void testAglaonemaFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(aglaonemaFactoryClass.getModifiers()));
    }

    @Test
    public void testAglaonemaFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(aglaonemaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testAglaonemaFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = aglaonemaFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testAglaonemaFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = aglaonemaFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testAglaonemaFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = aglaonemaFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah");
        assertEquals(air.getKebutuhanAir(), "Rendah");
    }

    @Test
    public void testAglaonemaFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = aglaonemaFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaSedang");
        assertEquals(cahaya.getKebutuhanCahaya(), "Sedang");
    }
}
