package com.kelompok1.NanemKuy.init.factoryTest;

import com.kelompok1.NanemKuy.init.factory.SansevieriaFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SansevieriaFactoryTest {
    private Class<?> sansevieriaFactoryClass;
    private SansevieriaFactory sansevieriaFactory;

    @BeforeEach
    public void setUp() throws Exception {
        sansevieriaFactoryClass = Class.forName("com.kelompok1.NanemKuy.init.factory.SansevieriaFactory");
        sansevieriaFactory = new SansevieriaFactory();
    }

    @Test
    public void testSansevieriaFactoryIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(sansevieriaFactoryClass.getModifiers()));
    }

    @Test
    public void testSansevieriaFactoryIsAFactory() {
        Collection<Type> interfaces = Arrays.asList(sansevieriaFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.factory.Factory")));
    }

    @Test
    public void testSansevieriaFactoryOverridecreateKebutuhanAirMethod() throws Exception {
        Method createKebutuhanAir = sansevieriaFactoryClass.getDeclaredMethod("createKebutuhanAir");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir",
                createKebutuhanAir.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanAir.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanAir.getModifiers()));
    }

    @Test
    public void testSansevieriaFactoryOverridecreateKebutuhanCahayaMethod() throws Exception {
        Method createKebutuhanCahaya = sansevieriaFactoryClass.getDeclaredMethod("createKebutuhanCahaya");
        assertEquals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya",
                createKebutuhanCahaya.getGenericReturnType().getTypeName());
        assertEquals(0,
                createKebutuhanCahaya.getParameterCount());
        assertTrue(Modifier.isPublic(createKebutuhanCahaya.getModifiers()));
    }

    @Test
    public void testSansevieriaFactoryReturnsRightKebutuhanAir() {
        KebutuhanAir air = sansevieriaFactory.createKebutuhanAir();
        assertEquals(air.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirSedang");
        assertEquals(air.getKebutuhanAir(), "Sedang");
    }

    @Test
    public void testSansevieriaFactoryReturnsRightKebutuhanCahaya() {
        KebutuhanCahaya cahaya = sansevieriaFactory.createKebutuhanCahaya();
        assertEquals(cahaya.getClass().getName(),
                "com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi");
        assertEquals(cahaya.getKebutuhanCahaya(), "Tinggi");
    }
}

