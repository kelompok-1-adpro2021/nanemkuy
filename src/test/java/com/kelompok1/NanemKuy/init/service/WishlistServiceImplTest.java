package com.kelompok1.NanemKuy.init.service;

import static org.mockito.Mockito.*;

import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.repository.WishlistRepository;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WishlistServiceImplTest {
    @Mock
    private TanamanRepository tanamanRepository;

    @Mock
    private WishlistRepository wishlistRepository;

    @InjectMocks
    private WishlistServiceImpl wishlistService;

    private Tanaman tanaman;

    @BeforeEach
    void setUp() {
        tanaman = new Adenium("Adenium", 20000, 100);
    }

    @Test
    void whenSaveToWishlistMethodIsCalledItShouldSaveTanamanToWishlist() {
        when(tanamanRepository.getTanamanByNama("Adenium")).thenReturn(tanaman);

        wishlistService.saveToWishlist("user", "Adenium");
        verify(tanamanRepository, times(1)).getTanamanByNama("Adenium");
        verify(wishlistRepository, times(1)).addTanamanToUserWishlist("user", tanaman);
    }


    @Test
    void whenDeleteFromWishlistMethodIsCalledItShouldDeleteTanamanFromWishlist() {
        when(tanamanRepository.getTanamanByNama("Adenium")).thenReturn(tanaman);

        wishlistService.deleteFromWishlist("user", "Adenium");
        verify(tanamanRepository, times(1)).getTanamanByNama("Adenium");
        verify(wishlistRepository, times(1)).deleteTanamanFromUserWishlist("user", tanaman);
    }

    @Test
    void whenGetUserWishlistMethodIsCalledItShouldReturnUserWishlist() {
        wishlistService.getUserWishlist("user");
        verify(wishlistRepository, times(1)).getWishlistByUser("user");
    }

    @Test
    void whenIsTanamanInWishlistMethodIsCalledItCheckIfTanamanIsInUserWishlistOrNot() {
        when(tanamanRepository.getTanamanByNama("Adenium")).thenReturn(tanaman);

        wishlistService.isTanamanInWishlist("user", "Adenium");
        verify(tanamanRepository, times(1)).getTanamanByNama("Adenium");
        verify(wishlistRepository, times(1)).isTanamanInUserWishlist("user", tanaman);
    }

}
