package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.repository.CartRepository;
import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PembelianServiceImplTest {
    @Mock
    private TanamanRepository tanamanRepository;

    @Mock
    private CartRepository cartRepository;

    @InjectMocks
    private PembelianServiceImpl pembelianService;

    Tanaman tanaman;

    @BeforeEach
    public void setUp() {
        tanaman = new Adenium("Adenium", 20000, 100);
    }

    @Test
    public void whenCreateProdukIsCalledItShouldCallTanamanRepositoryGetTanamanByNama() {
        pembelianService.createProduk("Adenium", 3);
        verify(tanamanRepository, times(1)).getTanamanByNama("Adenium");
    }


    @Test
    public void whenCreateProdukIsCalledItShouldCallOtherCreateProdukMethod() {
        Produk produk = pembelianService.createProduk("Adenium", 3, 2, 1, 1, 3);
    }

    @Test
    public void whenSaveProdukIsCalledItShouldCallTanamanRepositoryAddProduct() {
        Produk produk = new ProdukImpl(tanamanRepository.getTanamanByNama("Adenium"), 2);
        pembelianService.saveProduk("username", produk);
        verify(cartRepository, times(1)).addProduk(produk, "username");
    }

    @Test
    public void whenBeliProdukIsCalledItShouldDecreaseTanamanStok() {
        when(tanamanRepository.getTanamanByNama("Adenium")).thenReturn(tanaman);

        Tanaman tanaman = tanamanRepository.getTanamanByNama("Adenium");
        int stokLama = tanaman.getStok();
        pembelianService.beliProduk("Adenium", 2);
        assertEquals(stokLama - 2, tanaman.getStok());
    }

}
