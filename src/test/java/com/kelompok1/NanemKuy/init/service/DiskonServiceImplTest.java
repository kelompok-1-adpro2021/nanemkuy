package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.diskonEvent.Event;
import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Calathea;
import com.kelompok1.NanemKuy.init.tanaman.Hortensia;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DiskonServiceImplTest {
    private TanamanRepository tanamanRepository = new TanamanRepository();

    @Mock
    private Event event;

    @InjectMocks
    private DiskonService diskonService = new DiskonServiceImpl(tanamanRepository);

    @Test
    void testAddDiskonEventMethod() {
        diskonService.addDiskonEvent("Promo Akhir Bulan", 10000, "Harga lebih dari 20000");
        verify(event, times(1)).setDiskonEvent("Promo Akhir Bulan", 10000,
                "Harga lebih dari 20000");
        verify(event, times(1)).update();
    }

    @Test
    void testRemoveDiskonEventMethod() {
        diskonService.removeDiskonEvent();
        verify(event, times(1)).setDiskonEvent(null, 0 ,"remove");
        verify(event, times(1)).update();
    }

    @Test
    void testGetEventMethod() {
        Event eventGet = diskonService.getEvent();
        assertEquals(event, eventGet);
    }
}
