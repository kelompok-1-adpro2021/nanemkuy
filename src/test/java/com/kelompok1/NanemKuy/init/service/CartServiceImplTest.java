package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.cart.Cart;
import com.kelompok1.NanemKuy.init.cart.Gosend;
import com.kelompok1.NanemKuy.init.cart.JasaKurir;
import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.repository.CartRepository;
import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Calathea;
import com.kelompok1.NanemKuy.init.tanaman.Hortensia;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
public class CartServiceImplTest {
    @Mock
    private CartRepository cartRepository;

    @Mock
    private TanamanRepository tanamanRepository;

    @InjectMocks
    private CartServiceImpl cartService;

    Tanaman tanaman;
    JasaKurir jasaKurir;

    private Class<?> cartServiceClass;
    @BeforeEach
    public void setUp() throws Exception{
        cartServiceClass = Class.forName("com.kelompok1.NanemKuy.init.service.CartService");
    }


    @Test
    public void testCartServiceHasGetListCartMethod() throws Exception {
        Method getListCart = cartServiceClass.getDeclaredMethod("getListCart", String.class);
        int methodModifiers = getListCart.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getListCart.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Produk.class));
    }

    @Test
    public void testCartServiceHasGetListHistoryMethod() throws Exception {
        Method getListCart = cartServiceClass.getDeclaredMethod("getListHistory", String.class);
        int methodModifiers = getListCart.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getListCart.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(String.class));
    }

    @Test
    public void testCartServiceHasGetJasaKurirMethod() throws Exception {
        Method getJasaKurir = cartServiceClass.getDeclaredMethod("getJasaKurir");
        int methodModifiers = getJasaKurir.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCartServiceHasGetHargaProdukMethod() throws Exception {
        Method getHargaProduk = cartServiceClass.getDeclaredMethod("getHargaProduk", String.class);
        int methodModifiers = getHargaProduk.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCartServiceHasGetHargaTotalMethod() throws Exception {
        Method getHargaTotal = cartServiceClass.getDeclaredMethod("getHargaTotal", String.class);
        int methodModifiers = getHargaTotal.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }


    @Test
    public void testCartServiceSetJasaKurir() throws Exception {
        jasaKurir = new Gosend();
        cartService.setJasaKurir("Gosend");

        assertEquals("Gosend", jasaKurir.getMerk());

    }


}
