package com.kelompok1.NanemKuy.init.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Calathea;
import com.kelompok1.NanemKuy.init.tanaman.Hortensia;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TanamanServiceImplTest {
    private Class<?> tanamanServiceClass;

    @Spy
    TanamanRepository tanamanRepository = new TanamanRepository();

    @InjectMocks
    TanamanService tanamanService = new TanamanServiceImpl(tanamanRepository);

    @BeforeEach
    public void setUp() throws Exception {
        tanamanServiceClass = Class.forName("com.kelompok1.NanemKuy.init.service.TanamanServiceImpl");
    }

    @Test
    public void testTanamanServiceInitRepoMakeFiveTanaman() throws Exception {
        Method initMethod = tanamanServiceClass.getDeclaredMethod("initRepo");
        initMethod.setAccessible(true);
        initMethod.invoke(tanamanService);
        verify(tanamanRepository, times(5)).addTanaman(any(Tanaman.class));
    }

    @Test
    public void testTanamanServiceHasGetListTanamanMethod() throws Exception {
        Method getListTanaman = tanamanServiceClass.getDeclaredMethod("getListTanaman");
        int methodModifiers = getListTanaman.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getListTanaman.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Tanaman.class));
    }

    @Test
    public void testTanamanServiceGetListTanamanReturnCorrectTanamanAmount() {
        tanamanRepository.addTanaman(new Adenium("Kamboja Jepang", 20000, 10));
        tanamanRepository.addTanaman(new Calathea("Calathea Louisae", 40000, 20));
        tanamanRepository.addTanaman(new Calathea("Calathea Lancifolia", 30000, 30));
        tanamanRepository.addTanaman(new Hortensia("Hydrangea aspera", 50000,15));

        assertEquals(9, tanamanService.getListTanaman().size());
        verify(tanamanRepository, atLeastOnce()).getAllTanaman();
    }

    @Test
    public void testTanamanServiceHasGetTanamanByNamaMethod() throws Exception {
        Method getTanamanByNama = tanamanServiceClass.getDeclaredMethod("getTanamanByNama", String.class);
        int methodModifiers = getTanamanByNama.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTanamanServiceGetTanamanByNamaReturnCorrectTanaman() {
        Tanaman tanamanHortensia = new Hortensia("Hydrangea aspera", 50000,15);
        tanamanRepository.addTanaman(new Adenium("Kamboja Jepang", 20000, 10));
        tanamanRepository.addTanaman(new Calathea("Calathea Louisae", 40000, 20));
        tanamanRepository.addTanaman(new Calathea("Calathea Lancifolia", 30000, 30));
        tanamanRepository.addTanaman(tanamanHortensia);
        Tanaman tanamanResult = tanamanService.getTanamanByNama("Hydrangea aspera");

        assertEquals(tanamanResult, tanamanHortensia);
        verify(tanamanRepository, atLeastOnce()).getTanamanByNama(tanamanHortensia.getNama());
    }

    @Test
    public void testTanamanServiceHasTambahStokTanamanMethod() throws Exception {
        Method tambahStokTanaman = tanamanServiceClass.getDeclaredMethod("tambahStokTanaman", String.class, int.class);
        int methodModifiers = tambahStokTanaman.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTanamanServiceTambahStokTanamanCorrectlyImplemented() {
        int stokLama = 10;
        int tambahanStok = 15;
        Tanaman mockTanaman = new Adenium("Kamboja Jepang", 20000, stokLama);
        when(tanamanRepository.getTanamanByNama(any(String.class))).thenReturn(mockTanaman);
        tanamanService.tambahStokTanaman("Kamboja Jepang", tambahanStok);
        verify(tanamanRepository, atLeastOnce()).getTanamanByNama(any(String.class));
        assertEquals(mockTanaman.getStok(), stokLama + tambahanStok);
    }

}
