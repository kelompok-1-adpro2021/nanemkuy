package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HomalonemaMaggieTest {
    private Class<?> homalomenaMaggieClass;
    private HomalomenaMaggie homalomenaMaggie;

    @BeforeEach
    public void setUp() throws Exception {
        homalomenaMaggieClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.HomalomenaMaggie");
        homalomenaMaggie = new HomalomenaMaggie("HomalomenaMaggie", 150000, 5);
    }

    @Test
    public void testHomalomenaMaggieIsConcreteClass() {
        assertFalse(Modifier.isAbstract(homalomenaMaggieClass.getModifiers()));
    }

    @Test
    public void testHomalomenaMaggieIsTanaman() {
        Class<?> parentClass = homalomenaMaggieClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testHomalomenaMaggieDefaultConstructorWorkCorrectly() {
        assertEquals("HomalomenaMaggie", homalomenaMaggie.getNama());
        assertEquals(150000, homalomenaMaggie.getHarga());
        assertEquals(5, homalomenaMaggie.getStok());
        assertEquals("Rendah", homalomenaMaggie.getKebutuhanCahaya());
        assertEquals("Rendah", homalomenaMaggie.getKebutuhanAir());
    }
}
