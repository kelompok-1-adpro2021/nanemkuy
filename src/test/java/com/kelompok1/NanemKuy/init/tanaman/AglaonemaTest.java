package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class AglaonemaTest {
    private Class<?> aglaonemaClass;
    private Aglaonema aglaonema;

    @BeforeEach
    public void setUp() throws Exception {
        aglaonemaClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Aglaonema");
        aglaonema = new Aglaonema("Aglaonema", 60000, 5);
    }

    @Test
    public void testAglaonemaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(aglaonemaClass.getModifiers()));
    }

    @Test
    public void testAglaonemaIsTanaman() {
        Class<?> parentClass = aglaonemaClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testAglaonemaDefaultConstructorWorkCorrectly() {
        assertEquals("Aglaonema", aglaonema.getNama());
        assertEquals(60000, aglaonema.getHarga());
        assertEquals(5, aglaonema.getStok());
        assertEquals("Sedang", aglaonema.getKebutuhanCahaya());
        assertEquals("Rendah", aglaonema.getKebutuhanAir());
    }
}
