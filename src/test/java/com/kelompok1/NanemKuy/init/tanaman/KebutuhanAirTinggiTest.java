package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class KebutuhanAirTinggiTest {
    private Class<?> kebutuhanAirTinggiClass;
    private KebutuhanAirTinggi kebutuhanAirTinggi;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanAirTinggiClass = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi");
        kebutuhanAirTinggi = new KebutuhanAirTinggi();
    }

    @Test
    public void testKebutuhanAirTinggiIsKebutuhanAir() {
        Collection<Type> interfaces = Arrays.asList(kebutuhanAirTinggiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir")));
    }

    @Test
    public void testKebutuhanAirTinggiOverrideGetKebutuhanAir() throws Exception {
        Method getKebutuhanAir = kebutuhanAirTinggiClass.getDeclaredMethod("getKebutuhanAir");

        assertTrue(Modifier.isPublic(getKebutuhanAir.getModifiers()));
        assertFalse(Modifier.isAbstract(getKebutuhanAir.getModifiers()));
        assertEquals("java.lang.String", getKebutuhanAir.getGenericReturnType().getTypeName());
    }

    @Test
    public void testKebutuhanAirTinggiGetKebutuhanAirMethodReturnCorrectly() {
        assertEquals("Tinggi", kebutuhanAirTinggi.getKebutuhanAir());
    }
}
