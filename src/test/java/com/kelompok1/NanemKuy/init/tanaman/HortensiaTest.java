package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HortensiaTest {
    private Class<?> hortensiaClass;
    private Hortensia hortensia;

    @BeforeEach
    public void setUp() throws Exception {
        hortensiaClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Hortensia");
        hortensia = new Hortensia("Hortensia", 30000, 5);
    }

    @Test
    public void testHortensiaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(hortensiaClass.getModifiers()));
    }

    @Test
    public void testHortensiaIsTanaman() {
        Class<?> parentClass = hortensiaClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testHortensiaDefaultConstructorWorkCorrectly() {
        assertEquals("Hortensia", hortensia.getNama());
        assertEquals(30000, hortensia.getHarga());
        assertEquals(5, hortensia.getStok());
        assertEquals("Tinggi", hortensia.getKebutuhanCahaya());
        assertEquals("Tinggi", hortensia.getKebutuhanAir());
    }
}
