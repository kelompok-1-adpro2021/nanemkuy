package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaRendah;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class KebutuhanCahayaRendahTest {
    private Class<?> kebutuhanCahayaRendahClass;
    private KebutuhanCahayaRendah kebutuhanCahayaRendah;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanCahayaRendahClass = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaRendah");
        kebutuhanCahayaRendah = new KebutuhanCahayaRendah();
    }

    @Test
    public void testKebutuhanCahayaRendahIsKebutuhanCahaya() {
        Collection<Type> interfaces = Arrays.asList(kebutuhanCahayaRendahClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya")));
    }

    @Test
    public void testKebutuhanCahayaRendahOverrideGetKebutuhanCahaya() throws Exception {
        Method getKebutuhanCahaya = kebutuhanCahayaRendahClass.getDeclaredMethod("getKebutuhanCahaya");

        assertTrue(Modifier.isPublic(getKebutuhanCahaya.getModifiers()));
        assertFalse(Modifier.isAbstract(getKebutuhanCahaya.getModifiers()));
        assertEquals("java.lang.String", getKebutuhanCahaya.getGenericReturnType().getTypeName());
    }

    @Test
    public void testKebutuhanCahayaRendahGetKebutuhanCahayaMethodReturnCorrectly() {
        assertEquals("Rendah", kebutuhanCahayaRendah.getKebutuhanCahaya());
    }
}
