package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class PhilodendronTest {
    private Class<?> philodendronClass;
    private Philodendron philodendron;

    @BeforeEach
    public void setUp() throws Exception {
        philodendronClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Philodendron");
        philodendron = new Philodendron("Philodendron", 60000, 5);
    }

    @Test
    public void testPhilodendronIsConcreteClass() {
        assertFalse(Modifier.isAbstract(philodendronClass.getModifiers()));
    }

    @Test
    public void testPhilodendronIsTanaman() {
        Class<?> parentClass = philodendronClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testPhilodendronDefaultConstructorWorkCorrectly() {
        assertEquals("Philodendron", philodendron.getNama());
        assertEquals(60000, philodendron.getHarga());
        assertEquals(5, philodendron.getStok());
        assertEquals("Rendah", philodendron.getKebutuhanCahaya());
        assertEquals("Sedang", philodendron.getKebutuhanAir());
    }
}
