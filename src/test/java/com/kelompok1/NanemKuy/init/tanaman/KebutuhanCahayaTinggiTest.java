package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class KebutuhanCahayaTinggiTest {
    private Class<?> kebutuhanCahayaTinggiClass;
    private KebutuhanCahayaTinggi kebutuhanCahayaTinggi;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanCahayaTinggiClass = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi");
        kebutuhanCahayaTinggi = new KebutuhanCahayaTinggi();
    }

    @Test
    public void testKebutuhanCahayaTinggiIsKebutuhanCahaya() {
        Collection<Type> interfaces = Arrays.asList(kebutuhanCahayaTinggiClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya")));
    }

    @Test
    public void testKebutuhanCahayaTinggiOverrideGetKebutuhanCahaya() throws Exception {
        Method getKebutuhanCahaya = kebutuhanCahayaTinggiClass.getDeclaredMethod("getKebutuhanCahaya");

        assertTrue(Modifier.isPublic(getKebutuhanCahaya.getModifiers()));
        assertFalse(Modifier.isAbstract(getKebutuhanCahaya.getModifiers()));
        assertEquals("java.lang.String", getKebutuhanCahaya.getGenericReturnType().getTypeName());
    }

    @Test
    public void testKebutuhanCahayaTinggiGetKebutuhanCahayaMethodReturnCorrectly() {
        assertEquals("Tinggi", kebutuhanCahayaTinggi.getKebutuhanCahaya());
    }
}
