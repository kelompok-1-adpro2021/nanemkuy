package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaSedang;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class KebutuhanCahayaSedangTest {
    private Class<?> kebutuhanCahayaSedangClass;
    private KebutuhanCahayaSedang kebutuhanCahayaSedang;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanCahayaSedangClass = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaSedang");
        kebutuhanCahayaSedang = new KebutuhanCahayaSedang();
    }

    @Test
    public void testKebutuhanCahayaSedangIsKebutuhanCahaya() {
        Collection<Type> interfaces = Arrays.asList(kebutuhanCahayaSedangClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya")));
    }

    @Test
    public void testKebutuhanCahayaSedangOverrideGetKebutuhanCahaya() throws Exception {
        Method getKebutuhanCahaya = kebutuhanCahayaSedangClass.getDeclaredMethod("getKebutuhanCahaya");

        assertTrue(Modifier.isPublic(getKebutuhanCahaya.getModifiers()));
        assertFalse(Modifier.isAbstract(getKebutuhanCahaya.getModifiers()));
        assertEquals("java.lang.String", getKebutuhanCahaya.getGenericReturnType().getTypeName());
    }

    @Test
    public void testKebutuhanCahayaSedangGetKebutuhanCahayaMethodReturnCorrectly() {
        assertEquals("Sedang", kebutuhanCahayaSedang.getKebutuhanCahaya());
    }
}
