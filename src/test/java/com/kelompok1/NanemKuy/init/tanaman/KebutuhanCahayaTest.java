package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KebutuhanCahayaTest {
    private Class<?> kebutuhanCahaya;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanCahaya = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya");
    }

    @Test
    public void testKebutuhanCahayaIsAPublicInterface() {
        int classModifier = kebutuhanCahaya.getModifiers();

        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }

    @Test
    public void testKebutuhanCahayaHasGetKebutuhanCahayaAbstractMethod() throws Exception {
        Method getKebutuhanCahaya = kebutuhanCahaya.getDeclaredMethod("getKebutuhanCahaya");
        int methodModifier = kebutuhanCahaya.getModifiers();

        assertTrue(Modifier.isPublic(methodModifier));
        assertTrue(Modifier.isAbstract(methodModifier));
        assertEquals(0, getKebutuhanCahaya.getParameterCount());
        assertEquals("java.lang.String", getKebutuhanCahaya.getGenericReturnType().getTypeName());
    }
}
