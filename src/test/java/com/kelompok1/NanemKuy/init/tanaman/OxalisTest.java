package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class OxalisTest {
    private Class<?> oxalisClass;
    private Oxalis oxalis;

    @BeforeEach
    public void setUp() throws Exception {
        oxalisClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Oxalis");
        oxalis = new Oxalis("Oxalis", 60000, 5);
    }

    @Test
    public void testOxalisIsConcreteClass() {
        assertFalse(Modifier.isAbstract(oxalisClass.getModifiers()));
    }

    @Test
    public void testOxalisIsTanaman() {
        Class<?> parentClass = oxalisClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testOxalisDefaultConstructorWorkCorrectly() {
        assertEquals("Oxalis", oxalis.getNama());
        assertEquals(60000, oxalis.getHarga());
        assertEquals(5, oxalis.getStok());
        assertEquals("Sedang", oxalis.getKebutuhanCahaya());
        assertEquals("Tinggi", oxalis.getKebutuhanAir());
    }
}
