package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirSedang;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class KebutuhanAirSedangTest {
    private Class<?> kebutuhanAirSedangClass;
    private KebutuhanAirSedang kebutuhanAirSedang;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanAirSedangClass = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirSedang");
        kebutuhanAirSedang = new KebutuhanAirSedang();
    }

    @Test
    public void testKebutuhanAirSedangIsKebutuhanAir() {
        Collection<Type> interfaces = Arrays.asList(kebutuhanAirSedangClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir")));
    }

    @Test
    public void testKebutuhanAirSedangOverrideGetKebutuhanAir() throws Exception {
        Method getKebutuhanAir = kebutuhanAirSedangClass.getDeclaredMethod("getKebutuhanAir");

        assertTrue(Modifier.isPublic(getKebutuhanAir.getModifiers()));
        assertFalse(Modifier.isAbstract(getKebutuhanAir.getModifiers()));
        assertEquals("java.lang.String", getKebutuhanAir.getGenericReturnType().getTypeName());
    }

    @Test
    public void testKebutuhanAirSedangGetKebutuhanAirMethodReturnCorrectly() {
        assertEquals("Sedang", kebutuhanAirSedang.getKebutuhanAir());
    }
}
