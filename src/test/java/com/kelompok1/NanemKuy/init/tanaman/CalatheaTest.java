package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CalatheaTest {
    private Class<?> calatheaClass;
    private Calathea calathea;

    @BeforeEach
    public void setUp() throws Exception {
        calatheaClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Calathea");
        calathea = new Calathea("Calathea", 15000, 5);
    }

    @Test
    public void testCalatheaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(calatheaClass.getModifiers()));
    }

    @Test
    public void testCalatheaIsTanaman() {
        Class<?> parentClass = calatheaClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testCalatheaDefaultConstructorWorkCorrectly() {
        assertEquals("Calathea", calathea.getNama());
        assertEquals(15000, calathea.getHarga());
        assertEquals(5, calathea.getStok());
        assertEquals("Sedang", calathea.getKebutuhanCahaya());
        assertEquals("Sedang", calathea.getKebutuhanAir());
    }
}
