package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class SansevieriaTest {
    private Class<?> sansevieriaClass;
    private Sansevieria sansevieria;

    @BeforeEach
    public void setUp() throws Exception {
        sansevieriaClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Sansevieria");
        sansevieria = new Sansevieria("Sansevieria", 60000, 5);
    }

    @Test
    public void testSansevieriaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(sansevieriaClass.getModifiers()));
    }

    @Test
    public void testSansevieriaIsTanaman() {
        Class<?> parentClass = sansevieriaClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testSansevieriaDefaultConstructorWorkCorrectly() {
        assertEquals("Sansevieria", sansevieria.getNama());
        assertEquals(60000, sansevieria.getHarga());
        assertEquals(5, sansevieria.getStok());
        assertEquals("Tinggi", sansevieria.getKebutuhanCahaya());
        assertEquals("Sedang", sansevieria.getKebutuhanAir());
    }
}
