package com.kelompok1.NanemKuy.init.tanaman;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TanamanTest {
    private Class<?> tanamanClass;
    private Tanaman tanaman;

    @BeforeEach
    public void setUp() throws Exception {
        tanamanClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Tanaman");
        tanaman = new Calathea("Calathea", 10000, 5);
    }

    @Test
    public void testTanamanIsAbstract() {assertTrue(Modifier.isAbstract(tanamanClass.getModifiers()));}

    @Test
    public void testTanamanGetNamaWorkCorrectly() throws Exception {
        Method getNama = tanamanClass.getDeclaredMethod("getNama");

        assertTrue(Modifier.isPublic(getNama.getModifiers()));
        assertEquals(0, getNama.getParameterCount());
        assertEquals("java.lang.String", getNama.getGenericReturnType().getTypeName());

        assertEquals("Calathea", tanaman.getNama());
    }

    @Test
    public void testTanamanGetHargaWorkCorrectly() throws Exception {
        Method getHarga = tanamanClass.getDeclaredMethod("getHarga");

        assertTrue(Modifier.isPublic(getHarga.getModifiers()));
        assertEquals(0, getHarga.getParameterCount());
        assertEquals("int", getHarga.getGenericReturnType().getTypeName());

        assertEquals(10000, tanaman.getHarga());
    }

    @Test
    public void testTanamanSetStokWorkCorrectly() throws Exception {
        Method setStok = tanamanClass.getDeclaredMethod("setStok",
                int.class);

        assertTrue(Modifier.isPublic(setStok.getModifiers()));
        assertEquals(1, setStok.getParameterCount());
        assertEquals("void", setStok.getGenericReturnType().getTypeName());

        Tanaman tanaman = new Calathea("Calathea", 10000, 5);
        tanaman.setStok(10);
        assertEquals(10, tanaman.getStok());
    }

    @Test
    public void testTanamanGetStokWorkCorrectly() throws Exception {
        Method getStok = tanamanClass.getDeclaredMethod("getStok");

        assertTrue(Modifier.isPublic(getStok.getModifiers()));
        assertEquals(0, getStok.getParameterCount());
        assertEquals("int", getStok.getGenericReturnType().getTypeName());

        assertEquals(5, tanaman.getStok());
    }

    @Test
    public void testTanamanSetKebutuhanAirWorkCorrectly() throws Exception {
        Method setKebutuhanAir = tanamanClass.getDeclaredMethod("setKebutuhanAir",
                KebutuhanAir.class);

        assertTrue(Modifier.isPublic(setKebutuhanAir.getModifiers()));
        assertEquals(1, setKebutuhanAir.getParameterCount());
        assertEquals("void", setKebutuhanAir.getGenericReturnType().getTypeName());

        Tanaman tanaman = new Calathea("Calathea", 10000, 5);
        tanaman.setKebutuhanAir(new KebutuhanAirTinggi());
        assertEquals("Tinggi", tanaman.getKebutuhanAir());
    }

    @Test
    public void testTanamanGetKebutuhanAirWorkCorrectly() throws Exception {
        Method getKebutuhanAir = tanamanClass.getDeclaredMethod("getKebutuhanAir");

        assertTrue(Modifier.isPublic(getKebutuhanAir.getModifiers()));
        assertEquals(0, getKebutuhanAir.getParameterCount());
        assertEquals("java.lang.String", getKebutuhanAir.getGenericReturnType().getTypeName());

        assertEquals("Sedang", tanaman.getKebutuhanAir());
    }

    @Test
    public void testTanamanSetKebutuhanCahayaWorkCorrectly() throws Exception {
        Method setKebutuhanCahaya = tanamanClass.getDeclaredMethod("setKebutuhanCahaya",
                KebutuhanCahaya.class);

        assertTrue(Modifier.isPublic(setKebutuhanCahaya.getModifiers()));
        assertEquals(1, setKebutuhanCahaya.getParameterCount());
        assertEquals("void", setKebutuhanCahaya.getGenericReturnType().getTypeName());

        Tanaman tanaman = new Calathea("Calathea", 10000, 5);
        tanaman.setKebutuhanCahaya(new KebutuhanCahayaTinggi());
        assertEquals("Tinggi", tanaman.getKebutuhanCahaya());
    }

    @Test
    public void testTanamanGetKebutuhanCahayaWorkCorrectly() throws Exception {
        Method getKebutuhanCahaya = tanamanClass.getDeclaredMethod("getKebutuhanCahaya");

        assertTrue(Modifier.isPublic(getKebutuhanCahaya.getModifiers()));
        assertEquals(0, getKebutuhanCahaya.getParameterCount());
        assertEquals("java.lang.String", getKebutuhanCahaya.getGenericReturnType().getTypeName());

        assertEquals("Sedang", tanaman.getKebutuhanCahaya());
    }
}
