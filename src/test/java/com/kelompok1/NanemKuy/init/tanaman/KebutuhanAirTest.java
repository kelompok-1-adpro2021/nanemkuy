package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class KebutuhanAirTest {

    private Class<?> kebutuhanAir;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanAir = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir");
    }

    @Test
    public void testKebutuhanAirIsAPublicInterface() {
        int classModifier = kebutuhanAir.getModifiers();

        assertTrue(Modifier.isPublic(classModifier));
        assertTrue(Modifier.isInterface(classModifier));
    }

    @Test
    public void testKebutuhanAirHasGetKebutuhanAirAbstractMethod() throws Exception {
        Method getKebutuhanAir = kebutuhanAir.getDeclaredMethod("getKebutuhanAir");
        int methodModifier = kebutuhanAir.getModifiers();

        assertTrue(Modifier.isPublic(methodModifier));
        assertTrue(Modifier.isAbstract(methodModifier));
        assertEquals(0, getKebutuhanAir.getParameterCount());
        assertEquals("java.lang.String", getKebutuhanAir.getGenericReturnType().getTypeName());
    }
}
