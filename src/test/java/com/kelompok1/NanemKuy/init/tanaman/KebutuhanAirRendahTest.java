package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class KebutuhanAirRendahTest {

    private Class<?> kebutuhanAirRendahClass;
    private KebutuhanAirRendah kebutuhanAirRendah;

    @BeforeEach
    public void setUp() throws Exception {
        kebutuhanAirRendahClass = Class.forName("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah");
        kebutuhanAirRendah = new KebutuhanAirRendah();
    }

   @Test
   public void testKebutuhanAirRendahIsKebutuhanAir() {
       Collection<Type> interfaces = Arrays.asList(kebutuhanAirRendahClass.getInterfaces());

       assertTrue(interfaces.stream()
               .anyMatch(type -> type.getTypeName()
               .equals("com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir")));
   }

   @Test
    public void testKebutuhanAirRendahOverrideGetKebutuhanAir() throws Exception {
        Method getKebutuhanAir = kebutuhanAirRendahClass.getDeclaredMethod("getKebutuhanAir");

        assertTrue(Modifier.isPublic(getKebutuhanAir.getModifiers()));
        assertFalse(Modifier.isAbstract(getKebutuhanAir.getModifiers()));
        assertEquals("java.lang.String", getKebutuhanAir.getGenericReturnType().getTypeName());
   }

   @Test
    public void testKebutuhanAirRendahGetKebutuhanAirMethodReturnCorrectly() {
       assertEquals("Rendah", kebutuhanAirRendah.getKebutuhanAir());
   }
}
