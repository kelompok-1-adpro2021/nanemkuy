package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MonsteraTest {
    private Class<?> monsteraClass;
    private Monstera monstera;

    @BeforeEach
    public void setUp() throws Exception {
        monsteraClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Monstera");
        monstera = new Monstera("Monstera", 30000, 5);
    }

    @Test
    public void testMonsteraIsConcreteClass() {
        assertFalse(Modifier.isAbstract(monsteraClass.getModifiers()));
    }

    @Test
    public void testMonsteraIsTanaman() {
        Class<?> parentClass = monsteraClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testMonsteraDefaultConstructorWorkCorrectly() {
        assertEquals("Monstera", monstera.getNama());
        assertEquals(30000, monstera.getHarga());
        assertEquals(5, monstera.getStok());
        assertEquals("Rendah", monstera.getKebutuhanCahaya());
        assertEquals("Tinggi", monstera.getKebutuhanAir());
    }
}
