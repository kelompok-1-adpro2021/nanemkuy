package com.kelompok1.NanemKuy.init.tanaman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class AdeniumTest {
    private Class<?> adeniumClass;
    private Adenium adenium;

    @BeforeEach
    public void setUp() throws Exception {
        adeniumClass = Class.forName("com.kelompok1.NanemKuy.init.tanaman.Adenium");
        adenium = new Adenium("Adenium", 60000, 5);
    }

    @Test
    public void testAdeniumIsConcreteClass() {
        assertFalse(Modifier.isAbstract(adeniumClass.getModifiers()));
    }

    @Test
    public void testAdeniumIsTanaman() {
        Class<?> parentClass = adeniumClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.tanaman.Tanaman",
                parentClass.getTypeName());
    }

    @Test
    public void testAdeniumDefaultConstructorWorkCorrectly() {
        assertEquals("Adenium", adenium.getNama());
        assertEquals(60000, adenium.getHarga());
        assertEquals(5, adenium.getStok());
        assertEquals("Tinggi", adenium.getKebutuhanCahaya());
        assertEquals("Rendah", adenium.getKebutuhanAir());
    }
}
