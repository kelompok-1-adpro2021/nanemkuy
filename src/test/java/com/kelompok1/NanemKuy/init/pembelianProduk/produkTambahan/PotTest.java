package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;


public class PotTest {
    private Class<?> potClass;
    private Produk produk;
    private Tanaman tanaman;
    private int jumlahTanaman;
    private int jumlahPot;

    @BeforeEach
    public void setUp() throws Exception {
        potClass = Class.forName("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.Pot");
        jumlahTanaman = 3;
        jumlahPot = 2;
        tanaman = new Adenium("Adenium", 20000, 100);
        produk = new ProdukImpl(tanaman, jumlahTanaman);
        produk = new Pot(produk, jumlahPot);
    }

    @Test
    public void testPotIsConcreteClass() {
        assertFalse(Modifier.isAbstract(potClass.getModifiers()));
    }

    @Test
    public void testPotIsProdukTambahan() {
        Class<?> parentClass = potClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.ProdukTambahan",
                parentClass.getName());
    }

    @Test
    public void testPotHasGetDeskripsiMethod() throws Exception {
        Method attack = potClass.getDeclaredMethod("getDeskripsi");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDeskripsiMethodIsImplementCorrectly() throws Exception {
        String deskripsi = tanaman.getNama() + ": " + jumlahTanaman + " buah";
        deskripsi = deskripsi + "\nPot: " + jumlahPot + " buah";
        assertEquals(produk.getDeskripsi(), deskripsi);
    }

    @Test
    public void testPotHasGetHargaMethod() throws Exception {
        Method attack = potClass.getDeclaredMethod("getHarga");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("int", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetHargaMethodIsImplementCorrectly() throws Exception {
        int totalHarga = tanaman.getHarga() * jumlahTanaman + 10000 * jumlahPot;
        assertEquals(produk.getHarga(), totalHarga);
    }
}
