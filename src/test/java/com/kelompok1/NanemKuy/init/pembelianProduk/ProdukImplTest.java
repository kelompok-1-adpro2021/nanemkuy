package com.kelompok1.NanemKuy.init.pembelianProduk;

import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProdukImplTest {
    private Class<?> produkImplClass;
    private Produk produk;
    private Tanaman tanaman;
    private int jumlahTanaman;

    @BeforeEach
    public void setUp() throws Exception {
        produkImplClass = Class.forName("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl");
        jumlahTanaman = 3;
        tanaman = new Adenium("Adenium", 20000, 100);
        produk = new ProdukImpl(tanaman, jumlahTanaman);
    }

    @Test
    public void testProdukImplIsConcreteClass() {
        assertFalse(Modifier.isAbstract(produkImplClass.getModifiers()));
    }

    @Test
    public void testProdukImplHasGetDeskripsiMethod() throws Exception {
        Method attack = produkImplClass.getDeclaredMethod("getDeskripsi");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDeskripsiMethodIsImplementCorrectly() {
        String deskripsi = tanaman.getNama() + ": " + jumlahTanaman + " buah";
        assertEquals(produk.getDeskripsi(), deskripsi);
    }

    @Test
    public void testProdukImplHasGetHargaMethod() throws Exception {
        Method attack = produkImplClass.getDeclaredMethod("getHarga");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("int", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetHargaMethodIsImplementCorrectly() {
        int totalHargaTanaman = tanaman.getHarga() * jumlahTanaman;
        assertEquals(produk.getHarga(), totalHargaTanaman);
    }

}
