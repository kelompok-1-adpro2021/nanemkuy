package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProdukTambahanTest {
    private Class<?> ProdukTambahanClass;

    @BeforeEach
    public void setUp() throws Exception {
        ProdukTambahanClass = Class.forName("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.ProdukTambahan");
    }

    @Test
    public void testProdukTambahanIsAbstractClass() {
        assertTrue(Modifier.isAbstract(ProdukTambahanClass.getModifiers()));
    }
}
