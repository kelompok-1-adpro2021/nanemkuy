package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MediaTanamTest {
    private Class<?> mediaTanamClass;
    private Produk produk;
    private Tanaman tanaman;
    private int jumlahTanaman;
    private int jumlahMediaTanam;

    @BeforeEach
    public void setUp() throws Exception {
        mediaTanamClass = Class.forName("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.MediaTanam");
        jumlahTanaman = 3;
        jumlahMediaTanam = 2;
        tanaman = new Adenium("Adenium", 20000, 100);
        produk = new ProdukImpl(tanaman, jumlahTanaman);
        produk = new MediaTanam(produk, jumlahMediaTanam);
    }

    @Test
    public void testMediaTanamIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mediaTanamClass.getModifiers()));
    }

    @Test
    public void testMediaTanamIsProdukTambahan() {
        Class<?> parentClass = mediaTanamClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.ProdukTambahan",
                parentClass.getName());
    }

    @Test
    public void testMediaTanamHasGetDeskripsiMethod() throws Exception {
        Method attack = mediaTanamClass.getDeclaredMethod("getDeskripsi");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDeskripsiMethodIsImplementCorrectly() throws Exception {
        String deskripsi = tanaman.getNama() + ": " + jumlahTanaman + " buah";
        deskripsi = deskripsi + "\nMedia Tanam: " + jumlahMediaTanam + " buah";
        assertEquals(produk.getDeskripsi(), deskripsi);
    }

    @Test
    public void testMediaTanamHasGetHargaMethod() throws Exception {
        Method attack = mediaTanamClass.getDeclaredMethod("getHarga");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("int", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetHargaMethodIsImplementCorrectly() throws Exception {
        int totalHarga = tanaman.getHarga() * jumlahTanaman + 10000 * jumlahMediaTanam;
        assertEquals(produk.getHarga(), totalHarga);
    }
}
