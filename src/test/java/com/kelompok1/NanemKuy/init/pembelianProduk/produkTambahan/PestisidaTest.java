package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;


public class PestisidaTest {
    private Class<?> pestisidaClass;
    private Produk produk;
    private Tanaman tanaman;
    private int jumlahTanaman;
    private int jumlahPestisida;

    @BeforeEach
    public void setUp() throws Exception {
        pestisidaClass = Class.forName("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.Pestisida");
        jumlahTanaman = 3;
        jumlahPestisida = 2;
        tanaman = new Adenium("Adenium", 20000, 100);
        produk = new ProdukImpl(tanaman, jumlahTanaman);
        produk = new Pestisida(produk, jumlahPestisida);
    }

    @Test
    public void testPestisidaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(pestisidaClass.getModifiers()));
    }

    @Test
    public void testPestisidaIsProdukTambahan() {
        Class<?> parentClass = pestisidaClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.ProdukTambahan",
                parentClass.getName());
    }

    @Test
    public void testPestisidaHasGetDeskripsiMethod() throws Exception {
        Method attack = pestisidaClass.getDeclaredMethod("getDeskripsi");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetDeskripsiMethodIsImplementCorrectly() throws Exception {
        String deskripsi = tanaman.getNama() + ": " + jumlahTanaman + " buah";
        deskripsi = deskripsi + "\nPestisida: " + jumlahPestisida + " buah";
        assertEquals(produk.getDeskripsi(), deskripsi);
    }

    @Test
    public void testPestisidaHasGetHargaMethod() throws Exception {
        Method attack = pestisidaClass.getDeclaredMethod("getHarga");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("int", attack.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGetHargaMethodIsImplementCorrectly() throws Exception {
        int totalHarga = tanaman.getHarga() * jumlahTanaman + 20000 * jumlahPestisida;
        assertEquals(produk.getHarga(), totalHarga);
    }
}
