package com.kelompok1.NanemKuy.init.repository;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Calathea;
import com.kelompok1.NanemKuy.init.tanaman.Hortensia;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CartRepositoryTest {
    private CartRepository cartRepository;

    @Mock
    private HashMap<String, List<Produk>> hashProduk;
    private HashMap<String, List<String>> hashHistory;

    private List<Produk> produkList;
    private List<String> historyList;
    private String username;
    private String history;
    private ProdukImpl adenium;
    private ProdukImpl hortensia;

    @BeforeEach
    void setUp() {
        adenium = new ProdukImpl(new Adenium("Adenium", 20000, 100),2);
        hortensia = new ProdukImpl(new Hortensia("Hortensia", 30000, 25),18);
        username = "user";

        cartRepository = new CartRepository();
        hashProduk = new HashMap<>();
        hashHistory = new HashMap<>();

        produkList = new ArrayList<>();
        produkList.add(adenium);
        produkList.add(hortensia);
        hashProduk.put(username, produkList);

        historyList = new ArrayList<>();
        historyList.add("Hiztory");
        hashHistory.put(username, historyList);
    }

    @Test
    void testWhenAddProdukToUserCartMethodIsCalledItShouldAddProdukToCart() {
        int sizeUserWishlistLama = produkList.size();
        ReflectionTestUtils.setField(cartRepository, "hashProduk", hashProduk);
        cartRepository.addProduk(new ProdukImpl(new Calathea("Calathea", 50000, 20),4), username);
        int sizeUserWishlistBaru = produkList.size();
        assertEquals(sizeUserWishlistLama + 1, sizeUserWishlistBaru);
    }

    @Test
    void testAddProdukToUserCartWhenUserIsNotInListWillCreateNewUserCart() {
        int sizeWishlistLama = hashProduk.size();
        ReflectionTestUtils.setField(cartRepository, "hashProduk", hashProduk);
        cartRepository.addProduk(adenium, "newUser");
        int sizeWishlistBaru = produkList.size();
        assertEquals(sizeWishlistLama + 1, sizeWishlistBaru);
    }

    @Test
    void testWhenAddProdukToUserCartMethodIsCalledItShouldAddProdukToHistory() {
        int sizeUserWishlistLama = historyList.size();
        ReflectionTestUtils.setField(cartRepository, "hashHistory", hashHistory);
        cartRepository.addNewHistory("History", username);
        int sizeUserWishlistBaru = historyList.size();
        assertEquals(sizeUserWishlistLama + 1, sizeUserWishlistBaru);
    }

    @Test
    void testAddProdukToUserCartWhenUserIsNotInListWillCreateNewUserHistory() {
        int sizeWishlistLama = historyList.size();
        ReflectionTestUtils.setField(cartRepository, "hashHistory", hashHistory);
        cartRepository.addNewHistory("Harara", "newUser");
        int sizeWishlistBaru = produkList.size();
        assertEquals(sizeWishlistLama + 1, sizeWishlistBaru);
    }

    @Test
    void testAddProdukToUserCartWhenUserIsNotInListWillDelete() {
        int sizeWishlistLama = hashProduk.size();
        ReflectionTestUtils.setField(cartRepository, "hashProduk", hashProduk);
        cartRepository.removeCart(username);
        int sizeWishlistBaru = produkList.size();
        assertEquals(0, sizeWishlistBaru);
    }



}
