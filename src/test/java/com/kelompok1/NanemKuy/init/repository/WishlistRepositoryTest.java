package com.kelompok1.NanemKuy.init.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Calathea;
import com.kelompok1.NanemKuy.init.tanaman.Hortensia;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.ReflectionTestUtils;

class WishlistRepositoryTest {
    private WishlistRepository wishlistRepository;

    @Mock
    private Map<String, Set<Tanaman>> wishlist;

    private Set<Tanaman> sampleUserWishlist;
    private String username;
    private Tanaman adenium;
    private Tanaman hortensia;

    @BeforeEach
    void setUp() {
        adenium = new Adenium("Adenium", 20000, 100);
        hortensia = new Hortensia("Hortensia", 30000, 25);
        username = "user";

        wishlistRepository = new WishlistRepository();
        wishlist = new HashMap<>();

        sampleUserWishlist = new HashSet<>();
        sampleUserWishlist.add(adenium);
        sampleUserWishlist.add(hortensia);
        wishlist.put(username, sampleUserWishlist);
    }

    @Test
    void testWhenAddTanamanToUserWishlistMethodIsCalledItShouldAddTanamanToWishlist() {
        int sizeUserWishlistLama = sampleUserWishlist.size();
        ReflectionTestUtils.setField(wishlistRepository, "wishlist", wishlist);
        wishlistRepository.addTanamanToUserWishlist(username, new Calathea("Calathea", 50000, 20));
        int sizeUserWishlistBaru = sampleUserWishlist.size();
        assertEquals(sizeUserWishlistLama + 1, sizeUserWishlistBaru);
    }

    @Test
    void testAddTanamanToUserWishlistWhenUserIsNotInListWillCreateNewUserWishlist() {
        int sizeWishlistLama = wishlist.size();
        ReflectionTestUtils.setField(wishlistRepository, "wishlist", wishlist);
        wishlistRepository.addTanamanToUserWishlist("newUser", adenium);
        int sizeWishlistBaru = wishlist.size();
        assertEquals(sizeWishlistLama + 1, sizeWishlistBaru);
    }

    @Test
    void testWhenDeleteFromWishlistMethodIsCalledItShouldDeleteTanamanFromWishlist() {
        int sizeUserWishlistLama = sampleUserWishlist.size();
        ReflectionTestUtils.setField(wishlistRepository, "wishlist", wishlist);
        wishlistRepository.deleteTanamanFromUserWishlist(username,adenium);
        int sizeUserWishlistBaru = sampleUserWishlist.size();
        assertEquals(sizeUserWishlistLama - 1, sizeUserWishlistBaru);

    }

    @Test
    void testWhenGetUserWishlistMethodIsCalledItShouldReturnUserWishlist() {
        ReflectionTestUtils.setField(wishlistRepository, "wishlist", wishlist);
        Set<Tanaman> userWishlist = wishlistRepository.getWishlistByUser(username);
        assertEquals(userWishlist, sampleUserWishlist);
    }

    @Test
    void testWhenIsTanamanInWishlistMethodIsCalledItCheckIfTanamanIsInUserWishlistOrNot() {
        ReflectionTestUtils.setField(wishlistRepository, "wishlist", wishlist);
        boolean result1 = wishlistRepository.isTanamanInUserWishlist(username, adenium);
        assertEquals(result1, true);

        Tanaman calathea = new Calathea("Calathea", 50000, 20);
        boolean result2 = wishlistRepository.isTanamanInUserWishlist(username, calathea);
        assertEquals(result2, false);
    }

    @Test
    void testWhenIsTanamanInWishlistMethodIsCalledWithUserThatNotHaveWishlistItWillReturnFalse() {
        ReflectionTestUtils.setField(wishlistRepository, "wishlist", wishlist);
        boolean result1 = wishlistRepository.isTanamanInUserWishlist("newuser", adenium);
        assertEquals(result1, false);
    }
}
