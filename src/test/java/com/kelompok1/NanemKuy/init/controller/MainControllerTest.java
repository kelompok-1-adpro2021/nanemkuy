package com.kelompok1.NanemKuy.init.controller;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.NanemKuy.init.diskonEvent.Event;
import com.kelompok1.NanemKuy.init.service.CartServiceImpl;
import com.kelompok1.NanemKuy.init.service.DiskonServiceImpl;
import com.kelompok1.NanemKuy.init.service.TanamanServiceImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MainControllerTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private TanamanServiceImpl tanamanService;

    @MockBean
    private CartServiceImpl cartService;

    @MockBean
    private DiskonServiceImpl diskonService;

    private Tanaman tanaman;
    private Event event;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        tanaman = new Adenium("Adenium", 50000, 10);
        event = new Event(Arrays.asList(tanaman));
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testLandingPageUrl() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("landingPage"))
                .andExpect(view().name("init/landing"));
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testHomepageUrlWhenThereIsNoKeyword() throws Exception {
        when(tanamanService.getListTanaman()).thenReturn(Arrays.asList(tanaman));

        mockMvc.perform(get("/home"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("tanamanHome"))
                .andExpect(model().attributeExists("tanamans"))
                .andExpect(view().name("init/home"));
        verify(tanamanService, times(1)).getListTanaman();
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testHomepageUrlWhenThereIsAKeyword() throws Exception {
        when(tanamanService.getListTanaman()).thenReturn(Arrays.asList(tanaman));

        mockMvc.perform(get("/home")
                .param("keyword", "Adenium")
                .param("air", "rendah")
                .param("cahaya", "rendah"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("tanamanHome"))
                .andExpect(model().attributeExists("tanamans"))
                .andExpect(view().name("init/home"));
        verify(tanamanService, times(1)).getListByFilter("Adenium", "rendah", "rendah");
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testDetailsTanamanUrl() throws Exception {
        when(tanamanService.getTanamanByNama("Adenium")).thenReturn(tanaman);

        mockMvc.perform(get("/detail/" + tanaman.getNama())
                .param("namaTanaman", "Adenium"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("detailTanaman"))
                .andExpect(model().attributeExists("tanaman"))
                .andExpect(view().name("init/detailproduk"));

        verify(tanamanService, times(1)).getTanamanByNama("Adenium");
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testCartPageUrl() throws Exception {
        when(cartService.getListCart("user")).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/cart"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("cartPage"))
                .andExpect(model().attributeExists("carteu"))
                .andExpect(view().name("init/cart"));

        verify(cartService, times(1)).getListCart("user");
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testPaymentPageUrl() throws Exception {
        mockMvc.perform(post("/newMenu")
                .param("type", "gosend"))
                .andExpect(handler().methodName("paymentPage"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/confirm"));

        verify(cartService, times(1)).setJasaKurir("gosend");
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testCheckoutPageUrl() throws Exception {
        mockMvc.perform(post("/checkoutMenu"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("checkoutPage"))
                .andExpect(redirectedUrl("/home"));
        verify(cartService, times(1)).resetListCard("user");
    }
}
