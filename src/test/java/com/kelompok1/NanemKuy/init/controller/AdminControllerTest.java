package com.kelompok1.NanemKuy.init.controller;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.NanemKuy.init.diskonEvent.Event;
import com.kelompok1.NanemKuy.init.service.DiskonServiceImpl;
import com.kelompok1.NanemKuy.init.service.TanamanServiceImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AdminControllerTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private TanamanServiceImpl tanamanService;

    @MockBean
    private DiskonServiceImpl diskonService;

    private Tanaman tanaman;
    private Event event;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        tanaman = new Adenium("Adenium", 50000, 10);
        event = new Event(Arrays.asList(tanaman));
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenListTanamanUrlIsAccessedItCanShowAllTanaman() throws Exception {
        mockMvc.perform(get("/admin/list-tanaman"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("tanamanList"))
                .andExpect(model().attributeExists("tanamans"))
                .andExpect(view().name("init/list-tanaman"));
        verify(tanamanService, times(1)).getListTanaman();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenTambahStokTanamanUrlIsAccessedItCanShowTanaman() throws Exception {
        when(tanamanService.getTanamanByNama("Adenium")).thenReturn(tanaman);

        mockMvc.perform(get("/admin/tambah-stok/" + tanaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("tambahStokTanaman"))
                .andExpect(model().attributeExists("tanaman"))
                .andExpect(view().name("init/tambah-stok-tanaman"));
        verify(tanamanService, times(1)).getTanamanByNama(tanaman.getNama());
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenPostTambahStokTanamanIsAccessedItWillSaveProdukToCart() throws Exception {
        mockMvc.perform(post("/admin/tambah-stok/" + tanaman.getNama())
                .param("stokTambahan", "10"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("postTambahStokTanaman"))
                .andExpect(redirectedUrl("/admin/list-tanaman"));
        verify(tanamanService, times(1))
                .tambahStokTanaman(tanaman.getNama(), 10);
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenTambahProdukUrlIsAccessed() throws Exception {
        mockMvc.perform(get("/admin/tambah_produk"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("tambahProduk"))
                .andExpect(view().name("init/tambah_produk"));
    }

//    @WithMockUser(username = "admin", roles = "ADMIN")
//    @Test
//    void testWhenTambahTanamanUrlIsAccessed() throws Exception {
//        mockMvc.perform(post("/admin/tambah_tanaman")
//                .param("nama", "Lidah Buaya")
//                .param("air", "rendah")
//                .param("cahaya", "rendah")
//                .param("stok", "20")
//                .param("harga", "25000"))
//                .andExpect(handler().methodName("tambahTanaman"))
//                .andExpect(status().is3xxRedirection())
//                .andExpect(redirectedUrl("/admin/list-tanaman"));
//        verify(tanamanService, times(1))
//                .tambahTanaman("Lidah Buaya", "rendah", "rendah", 20, 25000);
//    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenEventUrlIsAccessed() throws Exception {
        when(diskonService.getEvent()).thenReturn(event);

        mockMvc.perform(get("/admin/event"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("event"))
                .andExpect(model().attributeExists("event"))
                .andExpect(view().name("init/event"));
        verify(diskonService, times(1)).getEvent();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenDeleteEventUrlIsAccessed() throws Exception {
        when(diskonService.getEvent()).thenReturn(event);

        mockMvc.perform(post("/admin/event"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("deleteEvent"))
                .andExpect(model().attributeExists("event"))
                .andExpect(view().name("init/event"));
        verify(diskonService, times(1)).getEvent();
        verify(diskonService, times(1)).removeDiskonEvent();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenTambahUrlIsAccessed() throws Exception {
        when(diskonService.getEvent()).thenReturn(event);

        mockMvc.perform(get("/admin/tambah-event"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("tambahEvent"))
                .andExpect(model().attributeExists("event"))
                .andExpect(view().name("init/add-event"));
        verify(diskonService, times(1)).getEvent();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    void testWhenTambahUrlIsAccessedWithPostMethod() throws Exception {
        mockMvc.perform(post("/admin/tambah-event")
                .param("namaEvent", "Promo Akhir Bulan")
                .param("hargaDiskon", "20000")
                .param("kriteriaDiskon", "Tanaman dengan harga lebih dari Rp. 50.000"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/event"))
                .andExpect(handler().methodName("postTambahEvent"));
        verify(diskonService, times(1)).addDiskonEvent(
                "Promo Akhir Bulan", 20000, "Tanaman dengan harga lebih dari Rp. 50.000");
    }
}
