package com.kelompok1.NanemKuy.init.controller;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.*;
import com.kelompok1.NanemKuy.init.service.PembelianServiceImpl;
import com.kelompok1.NanemKuy.init.service.TanamanServiceImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PembelianControllerTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private PembelianServiceImpl pembelianService;

    @MockBean
    private TanamanServiceImpl tanamanService;

    private Tanaman tanaman;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        tanaman = new Adenium("Adenium", 50000, 10);
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenBeliProdukUrlIsAccessedItCanMakeAProduk() throws Exception {
        Produk produk = new ProdukImpl(tanaman, 2);
        when(pembelianService.createProduk(tanaman.getNama(), 2)).thenReturn(produk);

        mockMvc.perform(post("/beli-produk/" + tanaman.getNama())
                .param("jumlahProduk", "2"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("beliProdukTanaman"))
                .andExpect(model().attributeExists("produk"))
                .andExpect(model().attributeExists("listProdukTambahan"))
                .andExpect(view().name("init/beli-produk"));
        verify(pembelianService, times(1)).createProduk(tanaman.getNama(), 2);
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenSuksesMenambahkanProdukIsAccessedItWillSaveProdukToRepository() throws Exception {
        Produk produk = new ProdukImpl(tanaman, 2);
        produk = new MediaTanam(produk,1);
        produk = new Pestisida(produk, 1);
        produk = new Pupuk(produk, 2);
        when(pembelianService.createProduk(tanaman.getNama(), 2, 1, 1, 0, 2)).thenReturn(produk);
        when(tanamanService.getTanamanByNama("Adenium")).thenReturn(tanaman);

        mockMvc.perform(post("/beli-produk/sukses-menambahkan-produk")
                .param("namaTanaman", "Adenium")
                .param("jumlahTanaman", "2")
                .param("jumlahMediaTanam", "1")
                .param("jumlahPestisida", "1")
                .param("jumlahPot", "0")
                .param("jumlahPupuk", "2"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("suksesMenambahkanProduk"))
                .andExpect(model().attributeExists("produk"))
                .andExpect(view().name("init/sukses-menambahkan-produk"));
        verify(pembelianService, times(1)).createProduk(tanaman.getNama(), 2, 1, 1, 0, 2);
        verify(pembelianService, times(1)).saveProduk("user", produk);
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testSuksesMenambahkanProdukUrlIsAccessedWhenStokIsNotEnough() throws Exception {
        Produk produk = new ProdukImpl(tanaman, 2);
        produk = new MediaTanam(produk,1);
        produk = new Pestisida(produk, 1);
        produk = new Pupuk(produk, 2);
        when(pembelianService.createProduk(tanaman.getNama(), 20, 1, 1, 0, 2)).thenReturn(produk);
        when(tanamanService.getTanamanByNama("Adenium")).thenReturn(tanaman);

        mockMvc.perform(post("/beli-produk/sukses-menambahkan-produk")
                .param("namaTanaman", "Adenium")
                .param("jumlahTanaman", "20")
                .param("jumlahMediaTanam", "1")
                .param("jumlahPestisida", "1")
                .param("jumlahPot", "0")
                .param("jumlahPupuk", "2"))
                .andExpect(handler().methodName("suksesMenambahkanProduk"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/detail/" + tanaman.getNama()));
    }

}
