package com.kelompok1.NanemKuy.init.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.NanemKuy.init.service.WishlistServiceImpl;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WishlistControllerTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private WishlistServiceImpl wishlistService;

    private Tanaman tanaman;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        tanaman = new Adenium("Adenium", 50000, 10);
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenWishlistIsAccessedItCanShowAllTanamanInWishlist() throws Exception {
        mockMvc.perform(get("/wishlist"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("wishlist"))
                .andExpect(model().attributeExists("listTanaman"))
                .andExpect(view().name("init/wishlist"));
        verify(wishlistService, times(1)).getUserWishlist("user");
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenCheckTanamanInWishlistUrlIsAccessed() throws Exception {
        when(wishlistService.isTanamanInWishlist("user", "Adenium")).thenReturn(true);

        mockMvc.perform(get("/check-if-tanaman-in-wishlist/" + tanaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("checkIfTanamanInWishlist"))
                .andExpect(result -> assertEquals(
                        "true", result.getResponse().getContentAsString()));
        verify(wishlistService, times(1)).isTanamanInWishlist("user", tanaman.getNama());
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenAddToWishlistIsAccessedItWillSaveTanamanToWishlist() throws Exception {
        mockMvc.perform(post("/add-to-wishlist/" + tanaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addToWishlist"))
                .andExpect(result -> assertEquals("Sukses Menambahkan Produk ke Wishlist",
                        result.getResponse().getContentAsString()));
        verify(wishlistService, times(1)).saveToWishlist("user", tanaman.getNama());
    }

    @WithMockUser(username = "user", roles = "USER")
    @Test
    void testWhenDeleteFromWishlistIsAccessedItWillDeleteTanamanFromWishlist() throws Exception {
        mockMvc.perform(post("/delete-from-wishlist/" + tanaman.getNama()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("deleteFromWishlist"))
                .andExpect(result -> assertEquals("Sukses Menghapus Produk dari Wishlist",
                        result.getResponse().getContentAsString()));
        verify(wishlistService, times(1)).deleteFromWishlist("user", tanaman.getNama());
    }
}
