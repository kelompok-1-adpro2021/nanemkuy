package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class JneTest {

    private Class<?> jneClass;

    @BeforeEach
    public void setUp() throws Exception {
        jneClass = Class.forName("com.kelompok1.NanemKuy.init.cart.Jne");
    }

    @Test
    public void testGosendIsConcreteClass() {
        assertFalse(Modifier.isAbstract(jneClass.getModifiers()));
    }

    @Test
    public void testGoSendIsJasaKurir() {
        Class<?> parentClass = jneClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.cart.JasaKurir",
                parentClass.getName());
    }

    @Test
    public void testJneGetAliasMethodGetLama() throws Exception {
        Method getAlias = jneClass.getDeclaredMethod("getMerk");

        assertEquals("java.lang.String", getAlias.getGenericReturnType().getTypeName());
        assertEquals(0, getAlias.getParameterCount());
    }

    @Test
    public void testNewJneUsesInstantDefault() {
        Jne jne = new Jne();

        assertEquals("com.kelompok1.NanemKuy.init.cart.ExpressShipping",
                jne.getShippingTime().getClass().getName());
    }

    @Test
    public void testNewJneUsesPremiumDefault() {
        Jne jne = new Jne();

        assertEquals("com.kelompok1.NanemKuy.init.cart.StandardShipping",
                jne.getShippingFare().getClass().getName());
    }
}
