package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExpressShippingTest {
    private Class<?> expressShippingClass;

    @BeforeEach
    public void setUp() throws Exception {
        expressShippingClass = Class.forName("com.kelompok1.NanemKuy.init.cart.ExpressShipping");
    }

    @Test
    public void testExpressShippingIsAShippingTime() {
        Collection<Type> interfaces = Arrays.asList(expressShippingClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.cart.ShippingTime")));
    }

    @Test
    public void testAttackWithGunOverrideAttackMethod() throws Exception {
        Method attack = expressShippingClass.getDeclaredMethod("getLama");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }
}
