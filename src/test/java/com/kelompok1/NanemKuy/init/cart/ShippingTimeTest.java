package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ShippingTimeTest {

    private Class<?> shippingTimeClass;

    @BeforeEach
    public void setUp() throws Exception {
        shippingTimeClass = Class.forName("com.kelompok1.NanemKuy.init.cart.ShippingTime");
    }

    @Test
    public void testShippingTimeIsAPublicInterface() {
        int classModifiers = shippingTimeClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testShippingTimeIsShipping() {
        Collection<Type> interfaces = Arrays.asList(shippingTimeClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.cart.Shipping")));
    }

    @Test
    public void testShippingTimeHasGetLamaAbstractMethod() throws Exception {
        Method attack = shippingTimeClass.getDeclaredMethod("getLama");
        int methodModifiers = attack.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, attack.getParameterCount());
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }

}
