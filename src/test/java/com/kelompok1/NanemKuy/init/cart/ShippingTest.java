package com.kelompok1.NanemKuy.init.cart;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;


public class ShippingTest {
    private Class<?> shippingClass;

    @BeforeEach
    public void setUp() throws Exception{
        shippingClass = Class.forName("com.kelompok1.NanemKuy.init.cart.Shipping");
    }

    @Test
    public void testShippingIsAPublicInterface() {
        int classModifiers = shippingClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testShippingHasGetTypeAbstractMethod() throws Exception {
        Method getType = shippingClass.getDeclaredMethod("getNama");
        int methodModifiers = getType.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getType.getParameterCount());
        assertEquals("java.lang.String", getType.getGenericReturnType().getTypeName());
    }


}
