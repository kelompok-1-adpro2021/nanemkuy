package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InstantShippingTest {
    private Class<?> instantShippingClass;

    @BeforeEach
    public void setUp() throws Exception {
        instantShippingClass = Class.forName("com.kelompok1.NanemKuy.init.cart.InstantShipping");
    }

    @Test
    public void testInstantShippingIsAShippingTime() {
        Collection<Type> interfaces = Arrays.asList(instantShippingClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.cart.ShippingTime")));
    }

    @Test
    public void testInstantShippingOverrideGetLamaMethod() throws Exception {
        Method attack = instantShippingClass.getDeclaredMethod("getLama");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals("java.lang.String", attack.getGenericReturnType().getTypeName());
    }
}
