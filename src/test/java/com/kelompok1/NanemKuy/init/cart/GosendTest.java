package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class GosendTest {

    private Class<?> goSendClass;

    @BeforeEach
    public void setUp() throws Exception {
        goSendClass = Class.forName("com.kelompok1.NanemKuy.init.cart.Gosend");
    }

    @Test
    public void testGosendIsConcreteClass() {
        assertFalse(Modifier.isAbstract(goSendClass.getModifiers()));
    }

    @Test
    public void testGoSendIsJasaKurir() {
        Class<?> parentClass = goSendClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.cart.JasaKurir",
                parentClass.getName());
    }

    @Test
    public void testGosendGetAliasMethodGetLama() throws Exception {
        Method getAlias = goSendClass.getDeclaredMethod("getMerk");

        assertEquals("java.lang.String", getAlias.getGenericReturnType().getTypeName());
        assertEquals(0, getAlias.getParameterCount());
    }

    @Test
    public void testNewGoSendUsesInstantDefault() {
        Gosend gocen = new Gosend();

        assertEquals("com.kelompok1.NanemKuy.init.cart.InstantShipping",
                gocen.getShippingTime().getClass().getName());
    }

    @Test
    public void testNewKnightAdventurerUsesArmorAsDefault() {
        Gosend gocen = new Gosend();

        assertEquals("com.kelompok1.NanemKuy.init.cart.PremiumShipping",
                gocen.getShippingFare().getClass().getName());
    }
}
