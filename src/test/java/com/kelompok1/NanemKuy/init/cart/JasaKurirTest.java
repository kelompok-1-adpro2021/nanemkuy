package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JasaKurirTest {
    private Class<?> jasaKurirClass;

    @BeforeEach
    public void setUp() throws Exception{
        jasaKurirClass = Class.forName("com.kelompok1.NanemKuy.init.cart.JasaKurir");
    }

    @Test
    public void testJasaKurirIsAbstract() {
        assertTrue(Modifier.isAbstract(jasaKurirClass.getModifiers()));
    }

    @Test
    public void testJasaKurirHasShippingFareSetter() throws Exception {
        Method setShippingFare = jasaKurirClass.getDeclaredMethod("setShippingFare",
                ShippingFare.class);

        assertTrue(Modifier.isPublic(setShippingFare.getModifiers()));
        assertEquals(1, setShippingFare.getParameterCount());
        assertEquals("void", setShippingFare.getGenericReturnType().getTypeName());
    }

    @Test
    public void testJasaKurirHasShippingFareGetter() throws Exception {
        Method getShippingFare = jasaKurirClass.getDeclaredMethod("getShippingFare");

        assertTrue(Modifier.isPublic(getShippingFare.getModifiers()));
        assertEquals(0, getShippingFare.getParameterCount());
        assertEquals(ShippingFare.class, getShippingFare.getGenericReturnType());
    }

    @Test
    public void testJasaKurirHasShippingTimeSetter() throws Exception {
        Method setShippingTime = jasaKurirClass.getDeclaredMethod("setShippingTime",
                ShippingTime.class);

        assertTrue(Modifier.isPublic(setShippingTime.getModifiers()));
        assertEquals(1, setShippingTime.getParameterCount());
        assertEquals("void", setShippingTime.getGenericReturnType().getTypeName());
    }

    @Test
    public void testJasaKurirHasShippingTimeGetter() throws Exception {
        Method getShippingTime = jasaKurirClass.getDeclaredMethod("getShippingTime");

        assertTrue(Modifier.isPublic(getShippingTime.getModifiers()));
        assertEquals(0, getShippingTime.getParameterCount());
        assertEquals(ShippingTime.class, getShippingTime.getGenericReturnType());
    }

}
