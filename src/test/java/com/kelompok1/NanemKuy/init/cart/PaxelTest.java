package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class PaxelTest {

    private Class<?> paxelClass;

    @BeforeEach
    public void setUp() throws Exception {
        paxelClass = Class.forName("com.kelompok1.NanemKuy.init.cart.Paxel");
    }

    @Test
    public void testGosendIsConcreteClass() {
        assertFalse(Modifier.isAbstract(paxelClass.getModifiers()));
    }

    @Test
    public void testGoSendIsJasaKurir() {
        Class<?> parentClass = paxelClass.getSuperclass();

        assertEquals("com.kelompok1.NanemKuy.init.cart.JasaKurir",
                parentClass.getName());
    }

    @Test
    public void testGosendGetAliasMethodGetLama() throws Exception {
        Method getAlias = paxelClass.getDeclaredMethod("getMerk");

        assertEquals("java.lang.String", getAlias.getGenericReturnType().getTypeName());
        assertEquals(0, getAlias.getParameterCount());
    }

    @Test
    public void testNewPaxelUsesInstantDefault() {
        Paxel pacel = new Paxel();

        assertEquals("com.kelompok1.NanemKuy.init.cart.InstantShipping",
                pacel.getShippingTime().getClass().getName());
    }

    @Test
    public void testNewPaxelUsesPremiumDefault() {
        Paxel pacel = new Paxel();

        assertEquals("com.kelompok1.NanemKuy.init.cart.StandardShipping",
                pacel.getShippingFare().getClass().getName());
    }
}
