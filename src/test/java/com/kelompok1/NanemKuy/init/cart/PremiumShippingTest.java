package com.kelompok1.NanemKuy.init.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PremiumShippingTest {

    private Class<?> premiumShippingClass;

    @BeforeEach
    public void setUp() throws Exception {
        premiumShippingClass = Class.forName("com.kelompok1.NanemKuy.init.cart.PremiumShipping");
    }

    @Test
    public void testStandardShippingIsAShippingFare() {
        Collection<Type> interfaces = Arrays.asList(premiumShippingClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("com.kelompok1.NanemKuy.init.cart.ShippingFare")));
    }

    @Test
    public void testInstantShippingOverrideGetLamaMethod() throws Exception {
        Method attack = premiumShippingClass.getDeclaredMethod("getHarga");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals("int", attack.getGenericReturnType().getTypeName());
    }

}
