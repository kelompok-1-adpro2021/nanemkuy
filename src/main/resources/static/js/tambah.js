$(document).ready(function() {

    // SUBMIT FORM
    $("#myForm").submit(function(event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();
        ajaxPost();
    });


    function ajaxPost(){

        // PREPARE FORM DATA
        var formData = {
            nama : $("#nama").val(),
            air :  $("#air").val(),
            cahaya :  $("#cahaya").val(),
            stok :  $("#stok").val(),
            harga :  $("#harga").val()
        }

        // DO POST
        $.ajax({
            type : "POST",
            contentType : "application/json",
            // url : window.location + "/tambah",
            url : "/tambah",
            data : JSON.stringify(formData),
            dataType : 'json',
            success : function(result) {
                if(result.status == "sukses"){
                    alert("Berhasil menambahkan tanaman " + result.data.nama)
                }
                else if(result.status == "gagal"){
                    alert("Tanaman " + result.data.nama + " sudah ada dalam data")
                }else{
                    alert("Terjadi error dalam menambahkan tanaman " + result.data.nama)
                }
                console.log(result);
            },
            error : function(e) {
                alert("Error!")
                console.log(JSON.stringify(formData));
                console.log("ERROR: ", e);
            }
        });

        // Reset FormData after Posting
        resetData();

    }

    function resetData(){
        $("#nama").val("");
        $("#air").val("Rendah");
        $("#cahaya").val("Rendah");
        $("#stok").val(1);
        $("#harga").val(10000);
    }
});