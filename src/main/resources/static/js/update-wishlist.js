var addButton = '<button id="add-to-wishlist" class="w-100 btn-secondary btn-sm text-center" onclick="addTanamanToWishlist()">' +
                '<i class="fa fa-heart" style="color:white"></i>  Tambahkan ke Wishlist</button>';
var deleteButton = '<button id="delete-from-wishlist" class="w-100 btn-secondary btn-sm text-center" onclick="deleteTanamanFromWishlist()">' +
                   '<i class="fa fa-heart" style="color:red"></i>  Hapus dari Wishlist</button>';

function addTanamanToWishlist() {
    var url = '/add-to-wishlist/' + namaTanaman;
    $.ajax({
        url: url,
        type : "POST",
        dataType: "text",
        success: function(hasil) {
            var wishlistSection = $('#update-wishlist');
            wishlistSection.empty();
            wishlistSection.append(deleteButton);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("error");
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function deleteTanamanFromWishlist() {
    console.log("start of delete function");
    var url = '/delete-from-wishlist/' + namaTanaman;
    $.ajax({
        url: url,
        type : "POST",
        dataType: "text",
        success: function(hasil) {
            var wishlistSection = $('#update-wishlist');
            wishlistSection.empty();
            wishlistSection.append(addButton);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("error");
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function isTanamanInUserWishlist() {
    var url = '/check-if-tanaman-in-wishlist/' + namaTanaman;
    $.ajax({
        url: url,
        success: function(hasil) {
            var wishlistSection = $('#update-wishlist');
            wishlistSection.empty();
            if (hasil == false) {
                wishlistSection.append(addButton);
            } else {
                wishlistSection.append(deleteButton);
            }
        }
    });
}

$(document).ready(function() {
    isTanamanInUserWishlist();
});
