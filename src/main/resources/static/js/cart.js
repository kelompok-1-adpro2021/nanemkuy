var deleteButton = '<button id="delete-from-wishlist" class="w-100 btn-secondary btn-sm text-center" onclick="deleteCartElement()">' +
    '<i></i>  Kosongkan Cart</button>';

function deleteCartElement() {
    var url = '/delete-cart/';
    var namaclass = '#keranjang';
    $.ajax({
        url: url,
        type : "POST",
        dataType: "text",
        success: function(hasil) {
            var cartSection = $(namaclass);
            //cartSection.append(deleteButton)
            cartSection.remove();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("error");
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}
function startCart() {
    var url = '/is-cart-kosong/';
    $.ajax({
        success: function(hasil) {
            var cartSection = $('#kranjang');
            if(hasil !== false)
                cartSection.append(deleteButton);
        }
    });
}

$(document).ready(function() {
    startCart();
});

