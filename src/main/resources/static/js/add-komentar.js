function addKomentar() {
    var nama = document.getElementById("formNama").value;
    var komentar = document.getElementById("formKomentar").value;

    var url = '/tukang-taman/detail/' + namaTukangTaman + '/add-review/' + "?nama=" + nama + "&komentar=" + komentar

    $.ajax({
        url: url,
        type : "POST",
        contentType: "application/json",
        data: {
            "nama" : nama,
            "komentar" : komentar
        },
        success: function(newReview) {
             var result = $('#komentarBaru');
             var res = newReview.split("!inibatas!");
             var namaReviewer = res[0];
             var komentarReviewer = res[1];

             result.empty();
             result.append(
             '<div class="card">' +
                '<div class="card-body">' +
                    '<h5 class="font-weight-bolder">' +  namaReviewer + '</h5>' +
                    '<h5>' + komentarReviewer + '</h5>' +
                '</div>' +
             '</div>' +
             '<br>'
             );
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("error");
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

