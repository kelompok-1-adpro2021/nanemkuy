package com.kelompok1.NanemKuy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages={"com.kelompok1.NanemKuy.auth", "com.kelompok1.NanemKuy.init", "com.kelompok1.NanemKuy.tukangtaman"})
@EnableJpaRepositories
public class NanemKuyApplication {

	public static void main(String[] args) {
		SpringApplication.run(NanemKuyApplication.class, args);
	}

}
