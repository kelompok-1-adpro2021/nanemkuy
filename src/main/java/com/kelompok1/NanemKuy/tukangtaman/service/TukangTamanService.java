package com.kelompok1.NanemKuy.tukangtaman.service;

import com.kelompok1.NanemKuy.tukangtaman.core.TukangTaman;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface TukangTamanService {
    List<TukangTaman> getListTukangTaman();

    TukangTaman getTukangTaman(String nama);

    String addReviewToTukangTaman(String namaTukangTaman, String nama, String komentar);

    void addTukangTaman(String namaTukangTaman, String deskripsiTukangTaman,
                        String kategoriTukangTaman);

    void updateTukangTaman(String namaTukangTamanLama, String namaTukangTaman,
                           String deskripsiTukangTaman);

    void deleteTukangTaman(String namaTukangTaman);
}
