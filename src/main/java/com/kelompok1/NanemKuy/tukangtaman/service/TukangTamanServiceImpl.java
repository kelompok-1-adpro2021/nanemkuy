package com.kelompok1.NanemKuy.tukangtaman.service;

import com.kelompok1.NanemKuy.tukangtaman.core.*;
import com.kelompok1.NanemKuy.tukangtaman.repository.TukangTamanRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class TukangTamanServiceImpl implements TukangTamanService {
    private TukangTamanRepository repository;

    public TukangTamanServiceImpl(TukangTamanRepository repository) {
        this.repository = repository;
        initRepo();
    }

    private void initRepo() {
        repository.addTukangTaman(new TamanAsri("Taman Asri",
                "hubungi whatsapp 087887332367"));
        repository.addTukangTaman(new TamanHijauDaun("Taman Hijau Daun",
                "hubungi line taman_hijaudaun"));
        repository.addTukangTaman(new TamanJayaAgung("Taman Jaya Agung",
                "hubungi dm instagram @tamanjayaagung"));
        repository.addTukangTaman(new TamanRumahmu("Taman Rumahmu",
                "hubungi wa 081234567890"));
        repository.addTukangTaman(new TamanSejukRindang("Taman Sejuk Rindang",
                "hubungi telp 081233239867"));
    }

    public List<TukangTaman> getListTukangTaman() {
        return repository.getListTukangTaman();
    }

    public TukangTaman getTukangTaman(String nama) {
        return repository.getTukangTaman(nama);
    }

    /**
     * Method untuk menambahkan review ke atribut review di objek TukangTaman.
     * @param namaTukangTaman nama TukangTaman
     * @param nama nama yang me-review
     * @param komentar review
     * @return
     */
    public String addReviewToTukangTaman(String namaTukangTaman, String nama, String komentar) {
        Review newReview = new Review(nama, komentar);
        TukangTaman tukangTaman = this.getTukangTaman(namaTukangTaman);
        Review addedReview = tukangTaman.addReview(newReview);
        String returnValue = addedReview.getNama() + "!inibatas!" + addedReview.getKomentar();
        return returnValue;
    }

    @Override
    public void addTukangTaman(String namaTukangTaman, String deskripsiTukangTaman,
                               String kategoriTukangTaman) {
        TukangTaman tukangTaman;
        switch (kategoriTukangTaman) {
            case "Taman Asri":
                tukangTaman = new TamanAsri(namaTukangTaman, deskripsiTukangTaman);
                break;
            case "Taman Hijau Daun":
                tukangTaman = new TamanHijauDaun(namaTukangTaman, deskripsiTukangTaman);
                break;
            case "Taman Jaya Agung":
                tukangTaman = new TamanJayaAgung(namaTukangTaman, deskripsiTukangTaman);
                break;
            case "Taman Rumahmu":
                tukangTaman = new TamanRumahmu(namaTukangTaman, deskripsiTukangTaman);
                break;
            case "Taman Sejuk Rindang":
                tukangTaman = new TamanSejukRindang(namaTukangTaman, deskripsiTukangTaman);
                break;
            default:
                tukangTaman = null;
        }
        repository.addTukangTaman(tukangTaman);
    }

    @Override
    public void updateTukangTaman(String namaTukangTamanLama, String namaTukangTaman,
                                  String deskripsiTukangTaman) {
        TukangTaman tukangTaman = repository.getTukangTaman(namaTukangTamanLama);
        tukangTaman.setNama(namaTukangTaman);
        tukangTaman.setDeskripsi(deskripsiTukangTaman);
    }

    @Override
    public void deleteTukangTaman(String namaTukangTaman) {
        TukangTaman tukangTaman = repository.getTukangTaman(namaTukangTaman);
        repository.deleteTukangTaman(tukangTaman);
    }
}
