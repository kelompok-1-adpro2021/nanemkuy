package com.kelompok1.NanemKuy.tukangtaman.controller;

import com.kelompok1.NanemKuy.tukangtaman.service.TukangTamanService;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping(path = "/tukang-taman/admin")
public class TukangTamanAdminController {

    @Autowired
    private TukangTamanService tukangTamanService;

    private final List<String> kategoriTaman = Arrays.asList("Taman Asri", "Taman Hijau Daun",
            "Taman Jaya Agung", "Taman Rumahmu", "Taman Sejuk Rindang");

    @GetMapping("/list-tukang-taman")
    public String listTukangTaman(Model model) {
        model.addAttribute("listTukangTaman", tukangTamanService.getListTukangTaman());
        return "tukangtaman/list-tukang-taman";
    }

    /**
     * Controller untuk menampilakan form untuk membuat objek Tukang Taman.
     */
    @GetMapping("/add-tukang-taman")
    public String addTukangTaman(Model model) {
        model.addAttribute("kategoriTaman", kategoriTaman);
        return "tukangtaman/add-tukang-taman";
    }

    /**
     * Controller untuk membuat dan menambahkan objek Tukang Taman.
     * @param namaTukangTaman Nama Tukang Taman
     * @param deskripsiTukangTaman Deskripsi Tukang Taman
     * @param kategoriTukangTaman Kategori Tukang Taman
     */
    @PostMapping("/add-tukang-taman")
    public String postAddTukangTaman(Model model, @RequestParam String namaTukangTaman,
                                     @RequestParam String deskripsiTukangTaman,
                                     @RequestParam String kategoriTukangTaman) {
        tukangTamanService.addTukangTaman(namaTukangTaman, deskripsiTukangTaman,
                kategoriTukangTaman);
        return "redirect:/tukang-taman/admin/list-tukang-taman";
    }

    /**
     * Controller untuk menampilakan form untuk mengubah objek Tukang Taman.
     */
    @GetMapping("/update-tukang-taman/{namaTukangTaman}")
    public String updateTukangTaman(@PathVariable("namaTukangTaman") String namaTukangTaman,
                                    Model model) {
        model.addAttribute("tukangTaman", tukangTamanService.getTukangTaman(namaTukangTaman));
        return "tukangtaman/update-tukang-taman";
    }

    /**
     * Controller untuk mengubah objek Tukang Taman.
     * @param namaTukangTaman Nama Tukang Taman
     * @param deskripsiTukangTaman Deskripsi Tukang Taman
     */
    @PostMapping("/update-tukang-taman/{namaTukangTaman}")
    public String postUpdateTukangTaman(@PathVariable("namaTukangTaman") String namaTukangTamanLama,
                                        @RequestParam String namaTukangTaman,
                                        @RequestParam String deskripsiTukangTaman) {
        tukangTamanService.updateTukangTaman(namaTukangTamanLama, namaTukangTaman,
                deskripsiTukangTaman);
        return "redirect:/tukang-taman/admin/list-tukang-taman";
    }

    /**
     * Controller untuk menampilakan form untuk mengubah objek Tukang Taman.
     */
    @GetMapping("/delete-tukang-taman/{namaTukangTaman}")
    public String deleteTukangTaman(@PathVariable("namaTukangTaman") String namaTukangTaman,
                                    Model model) {
        model.addAttribute("tukangTaman", tukangTamanService.getTukangTaman(namaTukangTaman));
        return "tukangtaman/delete-tukang-taman";
    }

    /**
     * Controller untuk mengubah objek Tukang Taman.
     * @param namaTukangTaman Nama Tukang Taman
     */
    @PostMapping("/delete-tukang-taman/{namaTukangTaman}")
    public String postDeleteTukangTaman(@PathVariable("namaTukangTaman") String namaTukangTaman) {
        tukangTamanService.deleteTukangTaman(namaTukangTaman);
        return "redirect:/tukang-taman/admin/list-tukang-taman";
    }
}
