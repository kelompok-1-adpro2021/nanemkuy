package com.kelompok1.NanemKuy.tukangtaman.controller;

import com.kelompok1.NanemKuy.tukangtaman.service.TukangTamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class TukangTamanController {

    @Autowired
    private TukangTamanService tukangTamanService;

    @GetMapping("/tukang-taman")
    public String tukangTamanHome(Model model) {
        model.addAttribute("listtukangtaman", tukangTamanService.getListTukangTaman());
        return "tukangtaman/index";
    }

    @GetMapping("/tukang-taman/detail/{namaTukangTaman}")
    public String detailTukangTaman(@PathVariable("namaTukangTaman") String namaTukangTaman,
                                    Model model) {
        model.addAttribute("tukangtaman", tukangTamanService.getTukangTaman(namaTukangTaman));
        return "tukangtaman/detailTukangTaman";
    }

    @PostMapping(value = "/tukang-taman/detail/{namaTukangTaman}/add-review")
    @ResponseBody
    public ResponseEntity<String> addReview(@RequestParam String nama,
                                            @RequestParam String komentar,
                                            @PathVariable(value = "namaTukangTaman")
                                                        String namaTukangTaman) {
        String review = tukangTamanService.addReviewToTukangTaman(namaTukangTaman, nama, komentar);
        return ResponseEntity.ok(review);
    }
}
