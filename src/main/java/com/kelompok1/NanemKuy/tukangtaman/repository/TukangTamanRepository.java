package com.kelompok1.NanemKuy.tukangtaman.repository;

import com.kelompok1.NanemKuy.tukangtaman.core.*;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class TukangTamanRepository {
    List<TukangTaman> listTukangTaman;

    public TukangTamanRepository() {
        this.listTukangTaman = new ArrayList<>();
    }

    public List<TukangTaman> getListTukangTaman() {
        return listTukangTaman;
    }

    /**
     * Method untuk mengambil objek TukangTaman berdasarkan namanya.
     * @param nama nama dari TukangTaman
     * @return objek TukangTaman
     */
    public TukangTaman getTukangTaman(String nama) {
        for (TukangTaman tukangTaman : listTukangTaman) {
            if (tukangTaman.getNama().equals(nama)) {
                return tukangTaman;
            }
        }
        return null;
    }

    public void addTukangTaman(TukangTaman tukangTaman) {
        listTukangTaman.add(tukangTaman);
    }

    public void deleteTukangTaman(TukangTaman tukangTaman) {
        listTukangTaman.remove(tukangTaman);
    }
}