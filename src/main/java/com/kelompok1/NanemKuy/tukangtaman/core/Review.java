package com.kelompok1.NanemKuy.tukangtaman.core;

public class Review {

    private String nama;
    private String komentar;

    public Review(String nama, String komentar) {
        this.nama = nama;
        this.komentar = komentar;
    }

    public String getNama() {
        return nama;
    }

    public String getKomentar() {
        return komentar;
    }

}