package com.kelompok1.NanemKuy.tukangtaman.core;

public class TukangTamanMinimalis implements Kategori {
    public TukangTamanMinimalis() {}

    public String getRincian() {
        return "Siap membantu membuat taman minimalis di rumahmu.";
    }

}