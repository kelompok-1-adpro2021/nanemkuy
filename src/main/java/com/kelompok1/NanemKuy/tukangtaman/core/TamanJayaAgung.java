package com.kelompok1.NanemKuy.tukangtaman.core;

public class TamanJayaAgung extends TukangTaman {

    public TamanJayaAgung(String nama, String deskripsi) {
        super(nama, deskripsi);
        this.setKategori(new TukangTamanKolam());
    }
}