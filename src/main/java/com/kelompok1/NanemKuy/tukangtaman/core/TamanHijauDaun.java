package com.kelompok1.NanemKuy.tukangtaman.core;

public class TamanHijauDaun extends TukangTaman {

    public TamanHijauDaun(String nama, String deskripsi) {
        super(nama, deskripsi);
        this.setKategori(new TukangTamanLandscape());
    }
}