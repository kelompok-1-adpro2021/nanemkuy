package com.kelompok1.NanemKuy.tukangtaman.core;

public class TamanRumahmu extends TukangTaman {

    public TamanRumahmu(String nama, String deskripsi) {
        super(nama, deskripsi);
        this.setKategori(new TukangTamanMinimalis());
    }
}