package com.kelompok1.NanemKuy.tukangtaman.core;

import java.util.ArrayList;
import java.util.List;

public abstract class TukangTaman {
    private String nama;
    private Kategori kategori;
    private String deskripsi;
    private List<Review> listReview;

    /**
     * Constructor untuk class TukangTaman.
     * @param nama nama objek TukangTaman
     * @param deskripsi deskripsi objek Tukang Taman
     */
    public TukangTaman(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.listReview = new ArrayList<>();
    }

    public String getNama() {
        return this.nama;
    }

    public String getDeskripsi() {
        return this.deskripsi;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return this.kategori.getRincian();
    }

    public Kategori getKategoriInKategoriType() {
        return this.kategori;
    }

    public List<Review> getListReview() {
        return this.listReview;
    }

    /**
     * Method untuk menambahkan review pada objek TukangTaman.
     * @param newReview objek Review baru yang ingin ditambahkan
     * @return objek Review yang ditambahkan
     */
    public Review addReview(Review newReview) {
        List<Review> currentList = this.listReview;
        currentList.add(newReview);
        return newReview;
    }






}