package com.kelompok1.NanemKuy.tukangtaman.core;

public class TukangTamanLandscape implements Kategori {
    public TukangTamanLandscape() {}

    public String getRincian() {
        return "Jasa membuat taman lansekap untuk halaman depan dan belakang rumah!";
    }
}