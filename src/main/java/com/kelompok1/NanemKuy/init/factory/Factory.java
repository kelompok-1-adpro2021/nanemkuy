package com.kelompok1.NanemKuy.init.factory;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;

public interface Factory {
    public KebutuhanAir createKebutuhanAir();
    public KebutuhanCahaya createKebutuhanCahaya();
}
