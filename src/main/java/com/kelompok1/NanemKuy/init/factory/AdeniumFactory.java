package com.kelompok1.NanemKuy.init.factory;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi;

public class AdeniumFactory implements Factory{
    @Override
    public KebutuhanAir createKebutuhanAir() {
        return new KebutuhanAirRendah();
    }

    @Override
    public KebutuhanCahaya createKebutuhanCahaya() {
        return new KebutuhanCahayaTinggi();
    }
}
