package com.kelompok1.NanemKuy.init.factory;

import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaSedang;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi;

public class OxalisFactory implements Factory{
    @Override
    public KebutuhanAir createKebutuhanAir() {
        return new KebutuhanAirTinggi();
    }

    @Override
    public KebutuhanCahaya createKebutuhanCahaya() {
        return new KebutuhanCahayaSedang();
    }
}
