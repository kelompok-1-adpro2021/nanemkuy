package com.kelompok1.NanemKuy.init.message;

import com.kelompok1.NanemKuy.init.tanaman.Tanaman;

public class Response {
    private String status;
    private TanamanTemp data;

    public Response(){

    }

    public Response(String status, TanamanTemp data){
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(TanamanTemp data) {
        this.data = data;
    }
}
