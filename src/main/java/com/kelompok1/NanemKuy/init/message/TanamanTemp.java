package com.kelompok1.NanemKuy.init.message;

public class TanamanTemp {
    private String nama;
    private String air;
    private String cahaya;
    private int stok;
    private int harga;

    public TanamanTemp(){}

    public TanamanTemp(String nama, String air, String cahaya, int stok, int harga) {
        this.nama = nama;
        this.air = air;
        this.cahaya = cahaya;
        this.stok = stok;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    public String getCahaya() {
        return cahaya;
    }

    public void setCahaya(String cahaya) {
        this.cahaya = cahaya;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }
}
