package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;

public class Pestisida extends ProdukTambahan {

    public Pestisida(Produk produk, int jumlahProduk) {
        super(produk, jumlahProduk);
    }

    @Override
    public String getDeskripsi() {
        return this.produk.getDeskripsi() + "\nPestisida: " + this.jumlahProduk + " buah";
    }

    @Override
    public int getHarga() {
        return this.produk.getHarga() + (jumlahProduk * 20000);
    }
}