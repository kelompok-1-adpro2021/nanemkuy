package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;

public abstract class ProdukTambahan implements Produk {
    protected Produk produk;
    protected int jumlahProduk;

    public ProdukTambahan(Produk produk, int jumlahProduk) {
        this.produk = produk;
        this.jumlahProduk = jumlahProduk;
    }

    @Override
    public abstract String getDeskripsi();

    @Override
    public abstract int getHarga();
}
