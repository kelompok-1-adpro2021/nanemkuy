package com.kelompok1.NanemKuy.init.pembelianProduk;

import com.kelompok1.NanemKuy.init.tanaman.Tanaman;

public class ProdukImpl implements Produk {
    protected Tanaman tanaman;
    protected int jumlahPembelian;

    public ProdukImpl(Tanaman tanaman, int jumlahPembelian) {
        this.tanaman = tanaman;
        this.jumlahPembelian = jumlahPembelian;
    }

    @Override
    public String getDeskripsi() {
        return tanaman.getNama() + ": " + this.jumlahPembelian + " buah";
    }

    @Override
    public int getHarga() {
        return tanaman.getHargaDiskon() * jumlahPembelian;
    }

    public String getNamaTanaman() {
        return tanaman.getNama();
    }

    public int getJumlahPembelian() {
        return jumlahPembelian;
    }
}