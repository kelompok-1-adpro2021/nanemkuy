package com.kelompok1.NanemKuy.init.pembelianProduk;

public interface Produk {
    String getDeskripsi();

    int getHarga();
}
