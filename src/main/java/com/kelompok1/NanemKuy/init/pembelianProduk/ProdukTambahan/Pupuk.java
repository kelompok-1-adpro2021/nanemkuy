package com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;

public class Pupuk extends ProdukTambahan {

    public Pupuk(Produk produk, int jumlahProduk) {
        super(produk, jumlahProduk);
    }

    @Override
    public String getDeskripsi() {
        return this.produk.getDeskripsi() + "\nPupuk: " + this.jumlahProduk + " buah";
    }

    @Override
    public int getHarga() {
        return this.produk.getHarga() + (jumlahProduk * 15000);
    }
}