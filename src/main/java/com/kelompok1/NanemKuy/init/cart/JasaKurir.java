package com.kelompok1.NanemKuy.init.cart;

public abstract class JasaKurir{
    private ShippingTime shippingTime;
    private ShippingFare shippingFare;

    public JasaKurir(){

    }

    public abstract String getMerk();


    public void setShippingTime(ShippingTime shippingTime){
        this.shippingTime = shippingTime;
    }

    public ShippingTime getShippingTime(){
        return this.shippingTime;
    }

    public void setShippingFare(ShippingFare shippingFare){
        this.shippingFare = shippingFare;
    }

    public ShippingFare getShippingFare(){
        return this.shippingFare;
    }

}
