package com.kelompok1.NanemKuy.init.cart;

public class Paxel extends JasaKurir{
    public Paxel(){
        this.setShippingTime(new InstantShipping());
        this.setShippingFare(new StandardShipping());
    }
    public String getMerk(){
        return "Paxel";
    }
}
