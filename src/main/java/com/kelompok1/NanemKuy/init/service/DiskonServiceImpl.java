package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.diskonEvent.Event;
import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import org.springframework.stereotype.Service;

@Service
public class DiskonServiceImpl implements DiskonService{
    private Event event;

    public DiskonServiceImpl(TanamanRepository tanamanRepository) {
        this.event = new Event(tanamanRepository.getAllTanaman());
    }

    @Override
    public void addDiskonEvent(String namaEvent, int hargaDiskon, String kriteriaDiskon) {
        this.event.setDiskonEvent(namaEvent, hargaDiskon, kriteriaDiskon);
        this.event.update();
    }

    @Override
    public void removeDiskonEvent() {
        this.event.setDiskonEvent(null, 0, "remove");
        this.event.update();
    }

    @Override
    public Event getEvent() {
        return this.event;
    }
}
