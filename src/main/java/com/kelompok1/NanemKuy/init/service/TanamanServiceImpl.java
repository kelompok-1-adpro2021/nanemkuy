package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.tanaman.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TanamanServiceImpl implements TanamanService{
    private TanamanRepository repo;

    public TanamanServiceImpl(TanamanRepository repo){
        this.repo = repo;
        initRepo();
    }

    private void initRepo(){
        repo.addTanaman(new Calathea("Calathea", 15000, 5));
        repo.addTanaman(new Monstera("Monstera", 21000, 5));
        repo.addTanaman(new Hortensia("Hortensia", 30000, 5));
        repo.addTanaman(new Adenium("Adenium", 60000, 5));
        repo.addTanaman(new HomalomenaMaggie("Homalomena Maggie", 150000, 5));
    }

    public List<Tanaman> getListTanaman(){
        return repo.getAllTanaman();
    }

    @Override
    public Tanaman getTanamanByNama(String nama) {
        return repo.getTanamanByNama(nama);
    }

    @Override
    public List<Tanaman> getListByFilter(String keyword, String air, String cahaya){
        return repo.findByFilter(keyword, air, cahaya);
    }

    public void tambahStokTanaman(String nama, int stokTambahan) {
        Tanaman tanaman = repo.getTanamanByNama(nama);
        int stokTanaman = tanaman.getStok();
        tanaman.setStok(stokTanaman + stokTambahan);
    }

    public boolean tambahTanaman(@Param("nama") String nama, @Param("air") String air, @Param("cahaya") String cahaya, @Param("stok") int stok, @Param("harga") int harga) {
        Tanaman tanaman;
        if(!repo.cekIfExist(nama)){
            if(air.equalsIgnoreCase("rendah")){
                if (cahaya.equalsIgnoreCase("rendah")){
                    tanaman = new HomalomenaMaggie(nama, harga, stok);
                }
                else if (cahaya.equalsIgnoreCase("sedang")) {
                    tanaman = new Aglaonema(nama, harga, stok);
                } else {
                    tanaman = new Adenium(nama, harga, stok);
                }
            }
            else if (air.equalsIgnoreCase("sedang")){
                if (cahaya.equalsIgnoreCase("rendah")){
                    tanaman = new Philodendron(nama, harga, stok);
                }
                else if (cahaya.equalsIgnoreCase("sedang")) {
                    tanaman = new Calathea(nama, harga, stok);
                } else {
                    tanaman = new Sansevieria(nama, harga, stok);
                }
            } else {
                if (cahaya.equalsIgnoreCase("rendah")){
                    tanaman = new Monstera(nama, harga, stok);
                }
                else if (cahaya.equalsIgnoreCase("sedang")) {
                    tanaman = new Oxalis(nama, harga, stok);
                } else {
                    tanaman = new Hortensia(nama, harga, stok);
                }
            }
            repo.addTanaman(tanaman);
            return true;
        }else{
            return false;
        }

    }
}
