package com.kelompok1.NanemKuy.init.service;

import java.util.List;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;

public interface TanamanService {
    List<Tanaman> getListTanaman();
    Tanaman getTanamanByNama(String nama);
    List<Tanaman> getListByFilter(String keyword, String air, String cahaya);
    void tambahStokTanaman(String nama, int stokTambahan);
    boolean tambahTanaman(String nama, String air, String cahaya, int stok, int harga);
}