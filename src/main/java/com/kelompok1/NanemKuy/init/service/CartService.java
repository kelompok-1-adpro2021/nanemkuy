package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.cart.Cart;
import com.kelompok1.NanemKuy.init.cart.JasaKurir;
import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;

import java.util.List;

public interface CartService {
    List<Produk> getListCart(String user);
    List<String> getListHistory(String user);
    JasaKurir getJasaKurir();
    void setJasaKurir(String name);
    int getHargaTotal(String user);
    int getHargaProduk(String user);
    void resetListCard(String user);
    void deleteCart(String user);
    boolean isKosong(String user);
}
