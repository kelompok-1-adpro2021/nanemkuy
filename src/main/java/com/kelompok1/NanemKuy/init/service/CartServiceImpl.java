package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.cart.*;
import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.repository.CartRepository;
import com.kelompok1.NanemKuy.init.tanaman.Adenium;
import com.kelompok1.NanemKuy.init.tanaman.Calathea;
import com.kelompok1.NanemKuy.init.tanaman.Hortensia;
import java.util.List;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService{
    private CartRepository repo;
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Authentication authentication = securityContext.getAuthentication();
    String user = null;




    public CartServiceImpl(CartRepository repo){
        this.repo = repo;
        initRepo();
    }

    public void createProduct(Produk produk){
        repo.addProduk(produk, "nanemkuy1");
    }

    private void initRepo(){
        createProduct(new ProdukImpl(new Adenium("Aglonema", 10000, 10), 8));
        createProduct(new ProdukImpl(new Hortensia("Mawar", 2000, 10), 18));
        createProduct(new ProdukImpl(new Calathea("Anthurium", 19000, 10), 8));
        createProduct(new ProdukImpl(new Adenium("Melati", 1000, 15), 12));
    }

    public void setJasaKurir(String name){
        JasaKurir jk = null;
        if(name.contains("Gosend"))
            jk = new Gosend();
        else if(name.contains("Paxel"))
            jk = new Paxel();
        else
            jk = new Jne();

        repo.setJasaKurir(jk);
    }

    public int getHargaTotal(String user){
        return repo.getHargaTotal(user);
    }

    public int getHargaProduk(String user){
        return repo.getHargaProduk(user);
    }

    public List<Produk> getListCart(String user){
        return repo.getAllProduk(user);
    }

    public List<String> getListHistory(String user){
        return repo.getAllHistory(user);
    }

    public JasaKurir getJasaKurir(){
        return repo.getKurir();
    }

    public void resetListCard(String user) {repo.removeCart(user);}

    public void deleteCart(String user){
        repo.deleteProduk(user);
    }

    @Override
    public boolean isKosong(String user) {
        if(repo.getAllProduk(user).size() == 0)
            return true;
        return false;
    }
}
