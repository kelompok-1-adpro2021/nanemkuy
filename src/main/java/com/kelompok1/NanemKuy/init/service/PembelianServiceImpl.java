package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukTambahan.*;
import com.kelompok1.NanemKuy.init.repository.CartRepository;
import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.springframework.stereotype.Service;

@Service
public class PembelianServiceImpl implements PembelianService {
    private CartRepository cartRepository;
    private TanamanRepository tanamanRepository;

    public PembelianServiceImpl(TanamanRepository tanamanRepository,
                                CartRepository cartRepository) {
        this.cartRepository = cartRepository;
        this.tanamanRepository = tanamanRepository;
    }

    @Override
    public Produk createProduk(String namaTanaman, int jumlah) {
        Tanaman tanaman = tanamanRepository.getTanamanByNama(namaTanaman);
        return new ProdukImpl(tanaman, jumlah);
    }

    @Override
    public Produk createProduk(String namaTanaman, int jumlahTanaman, int jumlahMediaTanam,
                               int jumlahPestisida, int jumlahPot, int jumlahPupuk) {
        Produk produk = createProduk(namaTanaman, jumlahTanaman);
        if (jumlahMediaTanam > 0) {
            produk = new MediaTanam(produk, jumlahMediaTanam);
        }
        if (jumlahPestisida > 0) {
            produk = new Pestisida(produk, jumlahPestisida);
        }
        if (jumlahPot > 0) {
            produk = new Pot(produk, jumlahPot);
        }
        if (jumlahPupuk > 0) {
            produk = new Pupuk(produk, jumlahPupuk);
        }
        return produk;
    }

    @Override
    public void saveProduk(String user, Produk produk) {
        cartRepository.addProduk(produk, user);
    }

    @Override
    public void beliProduk(String namaTanaman, int jumlah) {
        Tanaman tanaman = tanamanRepository.getTanamanByNama(namaTanaman);
        int stok = tanaman.getStok();
        tanaman.setStok(stok - jumlah);
    }
}

