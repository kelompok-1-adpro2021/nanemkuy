package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.diskonEvent.Event;

public interface DiskonService {

    void addDiskonEvent(String namaEvent, int hargaDiskon, String kriteriaDiskon);
    void removeDiskonEvent();
    Event getEvent();
}
