package com.kelompok1.NanemKuy.init.service;


import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;

public interface PembelianService {
    Produk createProduk(String namaTanaman, int jumlah);

    Produk createProduk(String namaTanaman, int jumlahTanaman, int jumlahMediaTanam,
                        int jumlahPestisida, int jumlahPot, int jumlahPupuk);

    void saveProduk(String user, Produk produk);

    void beliProduk(String namaTanaman, int jumlah);
}
