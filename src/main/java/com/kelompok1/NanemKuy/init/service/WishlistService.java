package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.Set;

public interface WishlistService {
    void saveToWishlist(String username, String namaTanaman);

    void deleteFromWishlist(String username, String namaTanaman);

    Set<Tanaman> getUserWishlist(String username);

    boolean isTanamanInWishlist(String username, String namaTanaman);
}
