package com.kelompok1.NanemKuy.init.service;

import com.kelompok1.NanemKuy.init.repository.TanamanRepository;
import com.kelompok1.NanemKuy.init.repository.WishlistRepository;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class WishlistServiceImpl implements WishlistService {
    private WishlistRepository wishlistRepository;
    private TanamanRepository tanamanRepository;

    public WishlistServiceImpl(TanamanRepository tanamanRepository,
                              WishlistRepository wishlistRepository) {
        this.wishlistRepository = wishlistRepository;
        this.tanamanRepository = tanamanRepository;
    }

    @Override
    public void saveToWishlist(String username, String namaTanaman) {
        Tanaman tanaman = tanamanRepository.getTanamanByNama(namaTanaman);
        wishlistRepository.addTanamanToUserWishlist(username, tanaman);
    }

    @Override
    public void deleteFromWishlist(String username, String namaTanaman) {
        Tanaman tanaman = tanamanRepository.getTanamanByNama(namaTanaman);
        wishlistRepository.deleteTanamanFromUserWishlist(username, tanaman);
    }

    @Override
    public Set<Tanaman> getUserWishlist(String username) {
        return wishlistRepository.getWishlistByUser(username);
    }

    @Override
    public boolean isTanamanInWishlist(String username, String namaTanaman) {
        Tanaman tanaman = tanamanRepository.getTanamanByNama(namaTanaman);
        return wishlistRepository.isTanamanInUserWishlist(username, tanaman);
    }
}
