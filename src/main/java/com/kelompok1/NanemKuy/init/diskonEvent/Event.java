package com.kelompok1.NanemKuy.init.diskonEvent;

import com.kelompok1.NanemKuy.init.tanaman.Tanaman;

import java.util.List;

public class Event {
    private List<Tanaman> tanaman;
    private DiskonEvent diskonEvent;

    public Event(List<Tanaman> tanaman) {
        this.tanaman = tanaman;
        this.diskonEvent = DiskonEvent.getDiskonEvent();
    }

    public void setDiskonEvent(String namaEvent, int hargaDiskon, String kriteriaDiskon) {
        this.diskonEvent.setDiskonEvent(namaEvent, hargaDiskon, kriteriaDiskon);
    }

    public void update() {
        for(int i = 0; i < this.tanaman.size(); i++) {
            this.tanaman.get(i).getState(this.diskonEvent);
        }
    }

    public DiskonEvent getDiskonEvent() {
        return this.diskonEvent;
    }
}
