package com.kelompok1.NanemKuy.init.diskonEvent;

public class DiskonEvent {
    private String namaEvent = null;
    private int hargaDiskon = 0;
    private String kriteriaDiskon = "";

    private static DiskonEvent diskonEvent = new DiskonEvent();

    private DiskonEvent() {}

    public void setDiskonEvent(String namaEvent, int hargaDiskon, String kriteriaDiskon) {
        this.namaEvent = namaEvent;
        this.hargaDiskon = hargaDiskon;
        this.kriteriaDiskon = kriteriaDiskon;
    }

    public static DiskonEvent getDiskonEvent() {
        return diskonEvent;
    }

    public String getNamaEvent() {
        return this.namaEvent;
    }

    public int getHargaDiskon() {
        return this.hargaDiskon;
    }

    public String getKriteriaDiskon() {
        return this.kriteriaDiskon;
    }
}
