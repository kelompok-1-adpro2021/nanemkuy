package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.factory.AdeniumFactory;
import com.kelompok1.NanemKuy.init.factory.AglaonemaFactory;

public class Aglaonema extends Tanaman {
    AglaonemaFactory factory = new AglaonemaFactory();

    public Aglaonema(String nama, int harga, int stok) {
        super(nama, harga, stok);
        this.buat();
    }

    @Override
    public void buat(){
        this.setKebutuhanAir(factory.createKebutuhanAir());
        this.setKebutuhanCahaya(factory.createKebutuhanCahaya());
    }
}
