package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.factory.PhilodendronFactory;

public class Philodendron extends Tanaman {
    PhilodendronFactory factory = new PhilodendronFactory();

    public Philodendron(String nama, int harga, int stok) {
        super(nama, harga, stok);
        this.buat();
    }

    @Override
    public void buat(){
        this.setKebutuhanAir(factory.createKebutuhanAir());
        this.setKebutuhanCahaya(factory.createKebutuhanCahaya());
    }
}
