package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.factory.SansevieriaFactory;

public class Sansevieria extends Tanaman {
    SansevieriaFactory factory = new SansevieriaFactory();

    public Sansevieria(String nama, int harga, int stok) {
        super(nama, harga, stok);
        this.buat();
    }

    @Override
    public void buat(){
        this.setKebutuhanAir(factory.createKebutuhanAir());
        this.setKebutuhanCahaya(factory.createKebutuhanCahaya());
    }
}
