package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.factory.AdeniumFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirRendah;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaTinggi;

public class Adenium extends Tanaman {
    AdeniumFactory factory = new AdeniumFactory();

    public Adenium(String nama, int harga, int stok) {
        super(nama, harga, stok);
        this.buat();
    }

    @Override
    public void buat(){
        this.setKebutuhanAir(factory.createKebutuhanAir());
        this.setKebutuhanCahaya(factory.createKebutuhanCahaya());
    }
}
