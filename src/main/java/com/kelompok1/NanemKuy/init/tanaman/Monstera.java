package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.factory.MonsteraFactory;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAirTinggi;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahayaRendah;

public class Monstera extends Tanaman {
    MonsteraFactory factory = new MonsteraFactory();

    public Monstera(String nama, int harga, int stok) {
        super(nama, harga, stok);
        this.buat();
    }

    @Override
    public void buat(){
        this.setKebutuhanAir(factory.createKebutuhanAir());
        this.setKebutuhanCahaya(factory.createKebutuhanCahaya());
    }
}
