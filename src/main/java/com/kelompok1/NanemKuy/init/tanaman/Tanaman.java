package com.kelompok1.NanemKuy.init.tanaman;

import com.kelompok1.NanemKuy.init.diskonEvent.DiskonEvent;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanAir.KebutuhanAir;
import com.kelompok1.NanemKuy.init.kebutuhan.kebutuhanCahaya.KebutuhanCahaya;
import com.kelompok1.NanemKuy.init.service.TanamanService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class Tanaman {

    private String nama;
    private KebutuhanAir kebutuhanAir;
    private KebutuhanCahaya kebutuhanCahaya;
    private int harga;
    private int stok;
    private String deskripsi;
    private int hargaDiskon;

    @Autowired
    private TanamanService tanamanService;

    public Tanaman(String nama, int harga, int stok) {
        this.nama = nama;
        this.harga = harga;
        this.hargaDiskon = harga;
        this.stok = stok;
    }

    public Tanaman(String nama, String air, String cahaya, int stok, int harga){
        tanamanService.tambahTanaman(nama, air, cahaya, stok, harga);
    }

    public String getNama() {
        return this.nama;
    }

    public int getHarga() {
        return this.harga;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public int getStok() {
        return this.stok;
    }

    public void setKebutuhanAir(KebutuhanAir kebutuhanAir) {
        this.kebutuhanAir = kebutuhanAir;
    }

    public String getKebutuhanAir() {
        return this.kebutuhanAir.getKebutuhanAir();
    }

    public void setKebutuhanCahaya(KebutuhanCahaya kebutuhanCahaya) {
        this.kebutuhanCahaya = kebutuhanCahaya;
    }

    public String getKebutuhanCahaya() {
        return this.kebutuhanCahaya.getKebutuhanCahaya();
    }

    public void setHargaDiskon(int diskon) {
        this.hargaDiskon = this.harga - diskon;
    }

    public int getHargaDiskon() {
        return this.hargaDiskon;
    }

    public void getState(DiskonEvent diskonEvent) {
        if (diskonEvent.getKriteriaDiskon().equals("Tanaman dengan harga lebih dari Rp. 25.000")) {
            if (this.harga > 25000) {
                this.setHargaDiskon(diskonEvent.getHargaDiskon());
            }
        } else if (diskonEvent.getKriteriaDiskon().equals("Tanaman dengan harga lebih dari Rp. 50.000")) {
            if (this.harga > 50000) {
                this.setHargaDiskon(diskonEvent.getHargaDiskon());
            }
        } else if (diskonEvent.getKriteriaDiskon().equals("Tanaman dengan harga lebih dari Rp. 100.000")) {
            if (this.harga > 100000) {
                this.setHargaDiskon(diskonEvent.getHargaDiskon());
            }
        } else {
            this.setHargaDiskon(diskonEvent.getHargaDiskon());
        }
    }

    abstract  public void buat();
}