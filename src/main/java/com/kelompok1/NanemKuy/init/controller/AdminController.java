package com.kelompok1.NanemKuy.init.controller;

import com.kelompok1.NanemKuy.init.service.DiskonService;
import com.kelompok1.NanemKuy.init.service.TanamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping(path = "/admin")
public class AdminController {
    @Autowired
    private TanamanService tanamanService;

    @Autowired
    private DiskonService diskonService;

    @GetMapping("/list-tanaman")
    public String tanamanList(Model model) {
        model.addAttribute("tanamans", tanamanService.getListTanaman());
        return "init/list-tanaman";
    }

    @GetMapping("/tambah-stok/{namaTanaman}")
    public String tambahStokTanaman(@PathVariable("namaTanaman") String namaTanaman,
                                    Model model) {
        model.addAttribute("tanaman", tanamanService.getTanamanByNama(namaTanaman));
        return "init/tambah-stok-tanaman";
    }

    @PostMapping("/tambah-stok/{namaTanaman}")
    public String postTambahStokTanaman(@PathVariable("namaTanaman") String namaTanaman,
                                        @RequestParam int stokTambahan) {
        tanamanService.tambahStokTanaman(namaTanaman, stokTambahan);
        return "redirect:/admin/list-tanaman";
    }

    @GetMapping("/tambah_produk")
    public String tambahProduk(){
        return "init/tambah_produk";
    }


//    @PostMapping("/tambah_tanaman")
//    public String tambahTanaman(@RequestParam String nama_tanaman, @RequestParam String air, @RequestParam String cahaya, @RequestParam int stok, @RequestParam int harga){
//        if(!nama-tanaman.equals("")) {
//            tanamanService.tambahTanaman(nama_tanaman, air, cahaya, stok, harga);
//        }
//
//        return "redirect:/admin/list-tanaman";
//    }

    @GetMapping("/event")
    public String event(Model model) {
        model.addAttribute("event", diskonService.getEvent());
        return "init/event";
    }

    @PostMapping("/event")
    public String deleteEvent(Model model) {
        diskonService.removeDiskonEvent();
        model.addAttribute("event", diskonService.getEvent());
        return "init/event";
    }

    @GetMapping("/tambah-event")
    public String tambahEvent(Model model) {
        model.addAttribute("event", diskonService.getEvent());
        return "init/add-event";
    }

    @PostMapping("/tambah-event")
    public String postTambahEvent(@RequestParam String namaEvent, int hargaDiskon, String kriteriaDiskon) {
        diskonService.addDiskonEvent(namaEvent, hargaDiskon, kriteriaDiskon);
        return "redirect:/admin/event";
    }
}