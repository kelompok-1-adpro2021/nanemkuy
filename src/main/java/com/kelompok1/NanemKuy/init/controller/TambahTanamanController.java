package com.kelompok1.NanemKuy.init.controller;

import com.kelompok1.NanemKuy.init.message.Response;
import com.kelompok1.NanemKuy.init.message.TanamanTemp;
import com.kelompok1.NanemKuy.init.service.TanamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TambahTanamanController {
    @Autowired
    private TanamanService tanamanService;

    @PostMapping(value = "/tambah")
    public Response postTanaman(@RequestBody TanamanTemp tanaman){
        Response response;

        String nama = tanaman.getNama();
        String air = tanaman.getAir();
        String cahaya = tanaman.getCahaya();
        int stok = tanaman.getStok();
        int harga = tanaman.getHarga();

        boolean sukses = tanamanService.tambahTanaman(nama, air, cahaya, stok, harga);

        if(sukses){
            return new Response("sukses", tanaman);
        }else{
            return new Response("gagal", tanaman);
        }
    }
}
