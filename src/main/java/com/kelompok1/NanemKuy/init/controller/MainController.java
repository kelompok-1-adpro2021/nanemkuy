package com.kelompok1.NanemKuy.init.controller;

import com.kelompok1.NanemKuy.init.service.CartService;
import com.kelompok1.NanemKuy.init.service.DiskonService;
import com.kelompok1.NanemKuy.init.service.TanamanService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {
    @Autowired
    private TanamanService tanamanService;
    @Autowired
    private CartService cartService;
    @Autowired
    private DiskonService diskonService;

    @GetMapping("/")
    public String landingPage() {
        return "init/landing";
    }

    @GetMapping("/home")
    public String tanamanHome(Model model, String keyword, String air, String cahaya){
        if(keyword != null) {
            model.addAttribute("tanamans", tanamanService.getListByFilter(keyword, air, cahaya));
        } else {
            model.addAttribute("tanamans", tanamanService.getListTanaman());
        }

        return "init/home";
    }

    @RequestMapping("/detail/{namaTanaman}")
    public String detailTanaman(@PathVariable("namaTanaman") String namaTanaman, Model model) {
        model.addAttribute("tanaman", tanamanService.getTanamanByNama(namaTanaman));
        return "init/detailproduk";
    }

    @GetMapping("/cart")
    public String cartPage(Model model, Authentication authentication){
        model.addAttribute("carteu", cartService.getListCart(authentication.getName()));
        return "init/cart";
    }

    @PostMapping("/newMenu")
    public String paymentPage(HttpServletRequest request){
        String type = request.getParameter("type");
        cartService.setJasaKurir(type);
        return "redirect:/confirm" ;
    }

    @GetMapping("/confirm")
    public String confirmPage(Model model, Authentication authentication){
        model.addAttribute("carts", cartService.getListCart(authentication.getName()));
        model.addAttribute("cartu", cartService);
        model.addAttribute("hargadoang", cartService.getHargaProduk(authentication.getName()));
        model.addAttribute("hargatotal", cartService.getHargaTotal(authentication.getName()));

        return "init/payment";
    }

    @GetMapping("/history")
    public String historyPage(Model model, Authentication authentication){
        model.addAttribute("carts", cartService.getListHistory(authentication.getName()));
        return "init/history";
    }

    @PostMapping("/checkoutMenu")
    public String checkoutPage(HttpServletRequest request, Authentication authentication){
        cartService.resetListCard(authentication.getName());
        return "redirect:/home" ;
    }

    @PostMapping(value = "/delete-cart/")
    @ResponseBody
    public ResponseEntity<String> deleteFromCart(Authentication authentication) {
        cartService.deleteCart(authentication.getName());
        return ResponseEntity.ok("Sukses Menghapus Produk dari Cart");
    }

    @GetMapping(value = "/is-cart-kosong/")
    @ResponseBody
    public ResponseEntity<Boolean> checkIfCartKosong(Authentication authentication) {
        return ResponseEntity.ok(cartService.isKosong(authentication.getName()));
    }
}