package com.kelompok1.NanemKuy.init.controller;

import com.kelompok1.NanemKuy.init.service.WishlistService;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class WishlistController {
    @Autowired
    private WishlistService wishlistService;

    /**
     * Controller untuk halaman wishlist.
     */
    @GetMapping("/wishlist")
    public String wishlist(Authentication authentication, Model model) {
        String username = authentication.getName();
        Set<Tanaman> listTanaman =  wishlistService.getUserWishlist(username);
        model.addAttribute("listTanaman", listTanaman);
        return "init/wishlist";
    }

    /**
     * Controller mengecek apakah tanaman ada di dalam wishlist user atau tidak.
     */
    @GetMapping(value = "/check-if-tanaman-in-wishlist/{tanaman}")
    @ResponseBody
    public ResponseEntity<Boolean> checkIfTanamanInWishlist(Authentication authentication,
                                        @PathVariable(value = "tanaman") String namaTanaman) {
        return ResponseEntity.ok(wishlistService.isTanamanInWishlist(
                                    authentication.getName(), namaTanaman));
    }

    /**
     * Controller menambahkan tanaman ke dalam wishlist user.
     */
    @PostMapping(value = "/add-to-wishlist/{tanaman}")
    @ResponseBody
    public ResponseEntity<String> addToWishlist(Authentication authentication,
                                        @PathVariable(value = "tanaman") String namaTanaman) {
        wishlistService.saveToWishlist(authentication.getName(), namaTanaman);
        return ResponseEntity.ok("Sukses Menambahkan Produk ke Wishlist");
    }

    /**
     * Controller menghapus tanaman dari dalam wishlist user.
     */
    @PostMapping(value = "/delete-from-wishlist/{tanaman}")
    @ResponseBody
    public ResponseEntity<String> deleteFromWishlist(Authentication authentication,
                                             @PathVariable(value = "tanaman") String namaTanaman) {
        wishlistService.deleteFromWishlist(authentication.getName(), namaTanaman);
        return ResponseEntity.ok("Sukses Menghapus Produk dari Wishlist");
    }
}
