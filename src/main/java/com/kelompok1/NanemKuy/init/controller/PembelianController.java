package com.kelompok1.NanemKuy.init.controller;

import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.service.PembelianService;
import com.kelompok1.NanemKuy.init.service.TanamanService;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PembelianController {
    @Autowired
    private PembelianService pembelianService;

    @Autowired
    private TanamanService tanamanService;

    /**
     * Controller untuk pembelian produk dan produk tambahan.
     * @param jumlahProduk jumlah produk yang ingin dibeli
     * @param namaTanaman nama tanaman yang ingin dibeli
     **/
    @PostMapping("/beli-produk/{nama-tanaman}")
    public String beliProdukTanaman(Model model, @RequestParam int jumlahProduk,
                                    @PathVariable(value = "nama-tanaman") String namaTanaman) {
        Produk produk = pembelianService.createProduk(namaTanaman, jumlahProduk);
        List<String> listProdukTambahan = Arrays.asList("MediaTanam", "Pestisida", "Pot", "Pupuk");
        model.addAttribute("produk", produk);
        model.addAttribute("listProdukTambahan", listProdukTambahan);
        return "init/beli-produk";
    }

    /**
     * Controller untuk menambahkan produk ke cart.
     * @param namaTanaman nama tanaman yang ingin dibeli
     * @param jumlahTanaman jumlah tanaman yang ingin dibeli
     * @param jumlahMediaTanam jumlah media tanam yang ingin dibeli
     * @param jumlahPestisida jumlah pestisida yang ingin dibeli
     * @param jumlahPot jumlah pot yang ingin dibeli
     * @param jumlahPupuk jumlah pupuk yang ingin dibeli
     */
    @PostMapping("/beli-produk/sukses-menambahkan-produk")
    public String suksesMenambahkanProduk(Model model, Authentication authentication, @RequestParam String namaTanaman,
                                          @RequestParam int jumlahTanaman,
                                          @RequestParam int jumlahMediaTanam,
                                          @RequestParam int jumlahPestisida,
                                          @RequestParam int jumlahPot,
                                          @RequestParam int jumlahPupuk) {
        Tanaman tanaman = tanamanService.getTanamanByNama(namaTanaman);
        if (tanaman.getStok() < jumlahTanaman) {
            return "redirect:/detail/" + namaTanaman;
        }

        Produk produk = pembelianService.createProduk(namaTanaman, jumlahTanaman, jumlahMediaTanam,
                jumlahPestisida,jumlahPot,jumlahPupuk);
        pembelianService.beliProduk(namaTanaman, jumlahTanaman);
        pembelianService.saveProduk(authentication.getName(), produk);
        model.addAttribute("produk", produk);
        return "init/sukses-menambahkan-produk";
    }
}