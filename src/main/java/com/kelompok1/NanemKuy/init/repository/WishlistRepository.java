package com.kelompok1.NanemKuy.init.repository;

import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Repository;

@Repository
public class WishlistRepository {
    private Map<String, Set<Tanaman>> wishlist;

    public WishlistRepository() {
        this.wishlist = new HashMap<>();
    }

    /**
     * Menambahkan suatu tanaman ke dalam wishlist user.
     * @param username username
     * @param tanaman objek tanaman yang ingin ditambahkan
     */
    public void addTanamanToUserWishlist(String username, Tanaman tanaman) {
        Set<Tanaman> userWishlist;
        if (wishlist.containsKey(username)) {
            userWishlist = wishlist.get(username);
        } else {
            userWishlist = new HashSet<>();
            wishlist.put(username, userWishlist);
        }
        userWishlist.add(tanaman);
    }

    public void deleteTanamanFromUserWishlist(String username, Tanaman tanaman) {
        Set<Tanaman> userWishlist = wishlist.get(username);
        userWishlist.remove(tanaman);
    }

    public Set<Tanaman> getWishlistByUser(String username) {
        return wishlist.get(username);
    }

    /**
     * Method untuk mengecek apakah suatu tanaman terdapat di dalam user wishlist atau tidak.
     * @param username username
     * @param tanaman objek tanaman yang dicari
     * @return true jika ada, false jika tidak ada
     */
    public boolean isTanamanInUserWishlist(String username, Tanaman tanaman) {
        if (wishlist.containsKey(username)) {
            Set<Tanaman> userWishlist = wishlist.get(username);
            return userWishlist.contains(tanaman);
        }
        return false;
    }
}

