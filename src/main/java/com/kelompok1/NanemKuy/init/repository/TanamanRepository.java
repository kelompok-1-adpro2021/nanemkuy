package com.kelompok1.NanemKuy.init.repository;

import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TanamanRepository {
    private Map<String, Tanaman> listTanaman;

    public TanamanRepository(){
        this.listTanaman = new HashMap<>();
    }

    public List<Tanaman> getAllTanaman() {
        return new ArrayList<Tanaman>(listTanaman.values());
    }

    public void addTanaman(Tanaman tanaman){
        String namaTanaman = tanaman.getNama();
        listTanaman.put(namaTanaman, tanaman);
    }

    public Tanaman getTanamanByNama(String nama) {
        return listTanaman.get(nama);
    }

    public List<Tanaman> findByFilter(@Param("keyword") String keyword, @Param("air") String air, @Param("cahaya") String cahaya) {
        List<Tanaman> listBaru = new ArrayList<>();
        for (Tanaman e : listTanaman.values()) {
            if(e.getNama().toLowerCase().contains(keyword.toLowerCase()) && e.getKebutuhanAir().contains(air) && e.getKebutuhanCahaya().contains(cahaya)){
                listBaru.add(e);
            }
        }
        return listBaru;
    }

    public boolean cekIfExist(String nama){
        boolean ada = false;
        for(String key : listTanaman.keySet()){
            if(nama.equalsIgnoreCase(key)){
                ada = true;
            }
        }
        return ada;
    }
}
