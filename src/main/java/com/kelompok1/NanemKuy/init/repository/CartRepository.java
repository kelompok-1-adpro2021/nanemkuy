package com.kelompok1.NanemKuy.init.repository;

import com.kelompok1.NanemKuy.auth.models.User;
import com.kelompok1.NanemKuy.auth.repository.UserRepository;
import com.kelompok1.NanemKuy.init.cart.Gosend;
import com.kelompok1.NanemKuy.init.cart.JasaKurir;
import com.kelompok1.NanemKuy.init.pembelianProduk.Produk;
import com.kelompok1.NanemKuy.init.pembelianProduk.ProdukImpl;
import com.kelompok1.NanemKuy.init.tanaman.Tanaman;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Repository
public class CartRepository {
    private List<Produk> listProduk;
    private List<String> listHistory;
    private JasaKurir jasaKurir;
    private HashMap<String,List<Produk>> hashProduk;
    private HashMap<String,List<String>> hashHistory;
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Authentication authentication = securityContext.getAuthentication();
    String username = null;



    public CartRepository(){
        this.listProduk = new ArrayList<>();
        this.jasaKurir = new Gosend();
        this.listHistory = new ArrayList<>();
        this.hashProduk = new HashMap<String,List<Produk>>();
        this.hashHistory = new HashMap<String,List<String>>();
    }

    public int getHargaTotal(String user){
        int total = getHargaProduk(user);
        total += jasaKurir.getShippingFare().getHarga();
        return total;
    }

//    public String getUserName(){
//        String uname = user.toString();
//        return uname;
//    }

    public int getHargaProduk(String user){
        List<Produk> produkList = hashProduk.get(user);
        int total = 0;
        for(Produk xx : produkList){
            total += xx.getHarga();
        }
        return total;
    }
    public void setJasaKurir(JasaKurir jasaKurir){
        this.jasaKurir = jasaKurir;
    }

    public List<Produk> getAllProduk(String user) {
        List<Produk> produkList = hashProduk.get(user);

        return produkList;
    }

    public List<String> getAllHistory(String user) {
        List<String> historyList = hashHistory.get(user);

        return historyList;
    }

    public JasaKurir getKurir() {
        return jasaKurir;
    }



    public Produk addProduk(Produk produk, String user){
        List<Produk> daftarProduk = hashProduk.get(user);

        if(daftarProduk == null){
            daftarProduk = new ArrayList<>();
            daftarProduk.add(produk);
            hashProduk.put(user, daftarProduk);
        }
        else{
            daftarProduk.add(produk);
        }

        return produk;
    }

    public void deleteProduk(String user){
        List<Produk> daftarProduk = hashProduk.get(user);
        daftarProduk.clear();
        }



    public String addNewHistory(String message, String user){
        List<String> daftarHistory = hashHistory.get(user);

        if(daftarHistory == null){
            daftarHistory = new ArrayList<>();
            daftarHistory.add(message);
            hashHistory.put(user, daftarHistory);
        }
        else{
            daftarHistory.add(message);
        }

        return message;
    }

    public List<Produk> removeCart(String user){
        List<Produk> daftarProduk = hashProduk.get(user);
        String ss = "";
        for(Produk xx : daftarProduk){
            ss += (xx.getDeskripsi() + ",");
        }
        ss += "\n\n";
        ss += "Total Harga : ";
        ss += getHargaTotal(user);

        this.addNewHistory(ss, user);
        daftarProduk.clear();
        return daftarProduk;
    }

    public String getUser(){
        if (authentication != null)
        {
            if (authentication.getPrincipal() instanceof UserDetails)
                username = ((UserDetails) authentication.getPrincipal()).getUsername();
            else if (authentication.getPrincipal() instanceof String)
                username = (String) authentication.getPrincipal();
        }

        return username;
    }
}

