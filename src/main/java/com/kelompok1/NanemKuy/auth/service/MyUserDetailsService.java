package com.kelompok1.NanemKuy.auth.service;

import com.kelompok1.NanemKuy.auth.models.MyUserDetails;
import com.kelompok1.NanemKuy.auth.models.User;
import com.kelompok1.NanemKuy.auth.repository.UserRepository;
import com.kelompok1.NanemKuy.auth.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;
import java.util.Arrays;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(userName);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));

        return user.map(MyUserDetails::new).get();
    }

    public User save(UserRegistrationDto registration) {
        User user = new User();
        user.setUserName(registration.getFirstName());
        user.setPassword(registration.getPassword());
        user.setRoles("ROLE_USER");
        user.setActive(true);
        return userRepository.save(user);
    }
}