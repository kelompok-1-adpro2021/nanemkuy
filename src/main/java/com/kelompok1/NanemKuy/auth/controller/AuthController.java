package com.kelompok1.NanemKuy.auth.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;

@Controller
public class AuthController {

    @Secured("ROLE_ADMIN")
    @GetMapping("/admin")
    public String admin() {
        return "init/admin_home";
    }

}