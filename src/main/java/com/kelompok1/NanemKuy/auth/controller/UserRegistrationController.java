package com.kelompok1.NanemKuy.auth.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kelompok1.NanemKuy.auth.models.User;
import com.kelompok1.NanemKuy.auth.service.MyUserDetailsService;
import com.kelompok1.NanemKuy.auth.repository.UserRepository;
import com.kelompok1.NanemKuy.auth.web.dto.UserRegistrationDto;

import java.util.Optional;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MyUserDetailsService userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
                                      BindingResult result) {

        Optional<User> existing = userRepository.findByUserName(userDto.getFirstName());
        if (existing.isPresent()) {
            result.rejectValue("firstName", null, "There is already an account registered with that username");
        }

        if (result.hasErrors()) {
            return "registration";
        }

        userService.save(userDto);
        return "redirect:/registration?success";
    }
}