# Tugas Kelompok Advanced Programming - A3
# NanemKuy

## status pipeline:
[![pipeline status](https://gitlab.com/kelompok-1-adpro2021/nanemkuy/badges/master/pipeline.svg)](https://gitlab.com/kelompok-1-adpro2021/nanemkuy/-/commits/master)

## status code coverage:
[![coverage report](https://gitlab.com/kelompok-1-adpro2021/nanemkuy/badges/master/coverage.svg)](https://gitlab.com/kelompok-1-adpro2021/nanemkuy/-/commits/master)

## link heroku app:
http://nanemkuy.herokuapp.com/
